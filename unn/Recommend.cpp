/*----------------------------------------------------------------------
  File     : recommend.cpp
  Contents : algorithm for computing recommendation scores for user-item combinatations 
  Author   : Koen.Verstrepen@gmail.com
  Update   : Feb 2014
----------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <iomanip>
#include <stdlib.h>
#include <tuple>
using namespace std;
#include "Model.h"

void readConfigFile(char * &configPath,string &trainLinksPath,string &testLinksPath,string &outputPath,string &algorithm,pair<unsigned int, unsigned int> &ks, vector<pair<double,unsigned int>> &lambdas){
  ifstream configFile(configPath);
  string line;
  unsigned int lineCount = 0;
  while(getline(configFile,line)){
    ++lineCount;
    string delimiter = ":";
    vector<string> splitted;
    size_t pos = 0;
    while ((pos = line.find(delimiter)) != string::npos) {
      string token;
      token = line.substr(0, pos);
      splitted.push_back(token);
      line.erase(0, pos + delimiter.length());
    }// end of while pos = line.find
    // remaining part of string is also a token
    splitted.push_back(line);
    if(lineCount==1) trainLinksPath = splitted[0];
    else if(lineCount==2) testLinksPath = splitted[0];
    else if(lineCount==3) outputPath = splitted[0];
    else if(lineCount==4) algorithm = splitted[0];
    else if(lineCount==5) {
      double userK = stof(splitted[0]);
      double itemK = stof(splitted[1]);
      ks = make_pair(userK,itemK);
    }
    else if(lineCount>=6){
      double lambda = stof(splitted[0]);
      unsigned int pid = stoi(splitted[1]);
      lambdas.push_back(make_pair(lambda,pid));
    }
  }//end of while getline
}//end of method bracket

int main(int argc, char *argv[])
{
  cout << "recommendation algorithms" << endl;
  cout << "by koen.verstrepen@gmail.com" << endl;
  cout << "http://adrem.uantwerpen.be/koen-verstrepen" << endl << endl;
  cout << "last update: Feb 2014" << endl << flush;
  
  if(argc < 2) {
    cerr << "usage: " << argv[0] << "config-file-path" << endl;
  }
  else {
    cout << "config file path: " << argv[1] << endl << flush;

    clock_t start = clock();
    string trainLinksPath;
    string testLinksPath;
    string outputPath;
    string algorithm;
    pair<unsigned int, unsigned int> ks; //(userK,itemK)
    vector<pair<double, unsigned int>> lambdas;
    readConfigFile(argv[1],trainLinksPath,testLinksPath,outputPath,algorithm,ks,lambdas);
    cout << "config file read" << endl << flush;
    
    Model recommendationModel;
    cout << "recommendationModel initialized" << endl << flush;
    recommendationModel.build(trainLinksPath);
    cout << "model built" << endl << flush;
    if(algorithm.compare("ENSEMBLE_aman")==0){
      cout << "algorithm is ENSEMBLE_aman" << endl << flush;
      recommendationModel.parameters = {0,0,0,0,0,(double)ks.first,(double)ks.second}; //first four parameters don't matter in this case. Default values are used.
      recommendationModel.lambdas = lambdas;
      cout << "parameters set" << endl << flush;
      recommendationModel.readTestUsers(testLinksPath);
      cout << "start to score test links" << endl << flush;
      start = clock();
      recommendationModel.ENSEMBLE_aman(outputPath); 
    }
    else if(algorithm.compare("ENSEMBLE_amau")==0){
      cout << "algorithm is ENSEMBLE_amau" << endl << flush;
      recommendationModel.parameters = {0,0,0,0,0,(double)ks.first,(double)ks.second}; //first four parameters don't matter in this case. Default values are used.
      recommendationModel.lambdas = lambdas;
      cout << "parameters set" << endl << flush;
      recommendationModel.readTestLinks(testLinksPath);
      cout << "start to score test links" << endl << flush;
      start = clock();
      recommendationModel.ENSEMBLE_amau(outputPath); 
    }
    else if(algorithm.compare("KUNN_amau")==0){
      cout << "algorithm is KUNN_amau" << endl << flush;
      recommendationModel.parameters = {-0.5,-0.5,-0.5,-0.5,1,(double)ks.first,(double)ks.second};
      cout << "parameters set" << endl << flush;
      recommendationModel.readTestLinks(testLinksPath);
      cout << "start to score test links" << endl << flush;
      start = clock();
      recommendationModel.KUNN_amau(outputPath); 
    }
    else if(algorithm.compare("KUNN_aman")==0){
      cout << "algorithm is KUNN_aman" << endl << flush;
      recommendationModel.parameters = {-0.5,-0.5,-0.5,-0.5,1,(double)ks.first,(double)ks.second};
      cout << "parameters set" << endl << flush;
      recommendationModel.readTestUsers(testLinksPath);
      cout << "start to score test links" << endl << flush;
      start = clock();
      recommendationModel.KUNN_aman(outputPath); 
    }
    else if(algorithm.compare("itemBased_amau")==0){
      cout << "algorithm is itemBased_amau" << endl << flush;
      recommendationModel.parameters = {0,-0.5,0,-0.5,1,(double)ks.first,(double)ks.second};
      cout << "parameters set" << endl << flush;
      recommendationModel.readTestLinks(testLinksPath);
      cout << "start to score test links" << endl << flush;
      start = clock();
      recommendationModel.itemBased_amau(outputPath); 
    }
    else if(algorithm.compare("itemBased_aman")==0){
      cout << "algorithm is itemBased_aman" << endl << flush;
      recommendationModel.parameters = {0,-0.5,0,-0.5,1,(double)ks.first,(double)ks.second};
      cout << "parameters set" << endl << flush;
      recommendationModel.readTestUsers(testLinksPath);
      cout << "start to score test links" << endl << flush;
      start = clock();
      recommendationModel.itemBased_aman(outputPath); 
    }
    else if(algorithm.compare("userBased_amau")==0){
      cout << "algorithm is userBased_amau" << endl << flush;
      recommendationModel.parameters = {-0.5,0,-0.5,0,0,(double)ks.first,(double)ks.second};
      cout << "parameters set" << endl << flush;
      recommendationModel.readTestLinks(testLinksPath);
      cout << "start to score test links" << endl << flush;
      start = clock();
      recommendationModel.userBased_amau(outputPath); 
    }
    else if(algorithm.compare("userBased_aman")==0){
      cout << "algorithm is userBased_aman" << endl << flush;
      recommendationModel.parameters = {-0.5,0,-0.5,0,1,(double)ks.first,(double)ks.second};
      cout << "parameters set" << endl << flush;
      recommendationModel.readTestUsers(testLinksPath);
      cout << "start to score test links" << endl << flush;
      start = clock();
      recommendationModel.userBased_aman(outputPath); 
    }
    else cout << "ERROR: test format not known!" << endl << flush;
    cout << "time for appending data: " << "\t[" << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl;
  }
  return 0;
}



