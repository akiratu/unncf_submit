#ifndef MODEL_H
#define MODEL_H

#include <stdio.h>
#include <list>
#include <string>
#include <set>
#include <map>
#include <vector>
using namespace std;

class Model {
public:
    Model():noUserNodes(0),noItemNodes(0),noPrefs(0){};
    ~Model(){};
    void build(string trainString);
    bool readLink(ifstream &linkfile, vector<string> &splitted);
    void readTestLinks(string fn);
    void readTestUsers(string fn); 
    void userBased_amau(string fn);
    void userBased_aman(string fn);
    void weight_userBased(ofstream &output,unsigned int testUser, unsigned int testItem, bool &firstline);
    void itemBased_amau(string fn);   
    void itemBased_aman(string fn);
    void weight_itemBased(ofstream &output,unsigned int testUser, unsigned int testItem, bool &firstline);
    void KUNN_amau(string outputString);
    void KUNN_aman(string outputString);
    void weight_KUNN(ofstream &output,unsigned int testUser, unsigned int testItem, bool &firstline);
    double halfWeight_KUNN(string nodeFlavour, unsigned int testNode, double partialPathWeight,unsigned int testUser,unsigned int testItem);
    void ENSEMBLE_amau(string outputString);
    void weight_ENSEMBLE(map<unsigned int,ofstream*> outputs,unsigned int testUser, unsigned int testItem, map<unsigned int, bool> &firstline);
    void ENSEMBLE_aman(string outputString);
    void comp_neighborsByUser();
    void comp_neighborsByUser(vector<double> altParameters);
    void comp_neighborsByItem();
    void comp_neighborsByItem(vector<double> altParameters);
    void comp_itemsByNeighbor();
    void comp_itemsByNeighbor(vector<double> altParameters);
    void printProgress(string entity, unsigned int noUsersDone,clock_t start);
    void printScore(ofstream &output,bool &firstline,double &score, unsigned int user, unsigned int item, string method);
    void printScore(string outputPrefix,bool &firstline,double &score, unsigned int user, unsigned int item, string method, unsigned int pid);
    // void calcTransposeCentralities_fast();

    FILE *trainFile;
    FILE *testFile;
    vector<unsigned int> users;
    vector<unsigned int> items;
    vector<unsigned int> testUsers;
    vector<unsigned int> testItems;
    unsigned int noUserNodes;
    unsigned int noItemNodes;
    unsigned int noPrefs; 
    vector<double> parameters;
    vector<pair<double,unsigned int>> lambdas;
    map<unsigned int,vector<unsigned int>> testItemsByUser;
    map<unsigned int,vector<unsigned int>> trainItemsByUser;
    map<unsigned int,vector<unsigned int>> trainUsersByItem;
    map<unsigned int,unsigned int> itemCount;
    map<unsigned int,unsigned int> userCount;
    map<unsigned int,double> itemDensity;
    map<unsigned int,double> userDensity;
    map<unsigned int, vector<pair<unsigned int,double>>> neighborsByUser;
    map<unsigned int, vector<pair<unsigned int,double>>> neighborsByItem;
    map<unsigned int, vector<pair<unsigned int,double>>> itemsByNeighbor;
    map<unsigned int, vector<pair<unsigned int,double>>> usersByNeighbor;
};

#endif
