 /*----------------------------------------------------------------------
  File     : Model.cpp
  Contents : operations on a occf data model
  Author   : Koen Verstrepen
  Update   : Feb 2014
----------------------------------------------------------------------*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <set>
#include <vector>
#include <map>
#include <list>
#include <time.h>
#include <algorithm>
#include <cstdlib>
#include <math.h>
#include <functional>
#include <iomanip>
#include <string>
using namespace std::placeholders;
#include "Model.h"

bool compare_pairs_second_bib(pair<unsigned int,double> pair1, pair<unsigned int,double> pair2){
  return (pair1.second > pair2.second);
}//end of method

bool compare_pairs_first_sib(pair<unsigned int,double> pair1, pair<unsigned int,double> pair2){
  return (pair1.first < pair2.first);
}//end of method

bool Model::readLink(ifstream &linkfile, vector<string> &splitted){
  string line;
  splitted.clear();

  if(getline(linkfile,line)){
    string delimiter = "\t";
    size_t pos = 0;
    while ((pos = line.find(delimiter)) != string::npos) {
      string token;
      token = line.substr(0, pos);
      splitted.push_back(token);
      line.erase(0, pos + delimiter.length());
    }
    // remaining part of string is also a token
    splitted.push_back(line);
    return true;
  }
  else return false;
}//end of method

void Model::build(string fn){
  ifstream linkfile(fn);
  vector<string> linkInfo;
  set<unsigned int> users_set;
  set<unsigned int> items_set;
  while(readLink(linkfile, linkInfo)){
    unsigned int user = atoi(linkInfo[0].c_str());
    unsigned int item = atoi(linkInfo[1].c_str());
    unsigned int rating = atoi(linkInfo[2].c_str());
    if(rating>=0){
      users_set.insert(user);
      items_set.insert(item);
      noPrefs++;
      trainItemsByUser[user].push_back(item);
      trainUsersByItem[item].push_back(user);
      userCount[user]++;
      itemCount[item]++;
    }
  }
  copy(users_set.begin(),users_set.end(),back_inserter(users));
  copy(items_set.begin(),items_set.end(),back_inserter(items));
  noUserNodes = users.size();
  noItemNodes = items.size();
  linkfile.close();
  map<unsigned int, vector<unsigned int>>::iterator mapit;
  for(mapit=trainItemsByUser.begin(); mapit!=trainItemsByUser.end(); ++mapit) sort(mapit->second.begin(), mapit->second.end());
  for(mapit=trainUsersByItem.begin(); mapit!=trainUsersByItem.end(); ++mapit) sort(mapit->second.begin(), mapit->second.end());
  sort(users.begin(), users.end());
  sort(items.begin(), items.end());
}//end method bracket

void Model::readTestLinks(string fn){
  cout << "start reading test links" << endl << flush;
  ifstream linkfile(fn);
  vector<string> linkInfo;
  set<unsigned int> users_set;
  set<unsigned int> items_set;
  while(readLink(linkfile, linkInfo)){
    unsigned int user = atoi(linkInfo[0].c_str());
    unsigned int item = atoi(linkInfo[1].c_str());
    unsigned int rating = atoi(linkInfo[2].c_str());
    if(rating!=3) testItemsByUser[user].push_back(item), users_set.insert(user), items_set.insert(item);
  }
  linkfile.close();
  map<unsigned int, vector<unsigned int>>::iterator mapit;
  for(mapit=testItemsByUser.begin(); mapit!=testItemsByUser.end(); ++mapit) sort(mapit->second.begin(), mapit->second.end());
  copy(users_set.begin(),users_set.end(),back_inserter(testUsers));
  copy(items_set.begin(),items_set.end(),back_inserter(testItems));
  sort(testUsers.begin(), testUsers.end());
  sort(testItems.begin(), testItems.end());
}//end method bracket

void Model::readTestUsers(string fn){
  cout << "start reading test users" << endl << flush;
  ifstream linkfile(fn);
  vector<string> linkInfo;
  set<unsigned int> users_set;
  while(readLink(linkfile, linkInfo)){
    unsigned int user = atoi(linkInfo[0].c_str());
    users_set.insert(user);
  }
  linkfile.close();
  map<unsigned int, vector<unsigned int>>::iterator mapit;
  copy(users_set.begin(),users_set.end(),back_inserter(testUsers));
  sort(testUsers.begin(), testUsers.end());
  testItems = items;
}//end method bracket

void Model::ENSEMBLE_amau(string outputString){
  comp_neighborsByUser({-0.5,0,-0.5,0,0});
  comp_itemsByNeighbor({0,-0.5,0,-0.5,1});

  vector<pair<double,unsigned int>>::iterator lamit;
  map<unsigned int,ofstream*> outputs;
  map<unsigned int, bool> firstline;
  for(lamit=lambdas.begin(); lamit!=lambdas.end(); ++lamit){
    ostringstream convert;
    convert << lamit->second;
    string str = outputString+convert.str()+".csv";
    outputs[lamit->second] = new ofstream(str);
    firstline[lamit->second] =  true;
  }

  // cout << "c1" << endl << flush;
  vector<unsigned int>::iterator testUserIterator;
  vector<unsigned int>::iterator testItemIterator;
  unsigned int linkCount = 0;
  clock_t start = clock();
  // cout << "c2" << endl << flush;
  for(testUserIterator=testUsers.begin(); testUserIterator!=testUsers.end(); ++testUserIterator){
    for(testItemIterator=testItemsByUser[*testUserIterator].begin(); testItemIterator!=testItemsByUser[*testUserIterator].end(); ++testItemIterator){
      // cout << "c3" << endl << flush;
      // cout << "test user: " << *testUserIterator << endl << flush;
      // cout << "test item: " << *testItemIterator << endl << flush;
      weight_ENSEMBLE(outputs,*testUserIterator, *testItemIterator, firstline);
      // cout << "cend" << endl << flush;
      ++linkCount;
      printProgress("links",linkCount,start);
    }//for test items
  }//for test users

}//end of method

void Model::weight_ENSEMBLE(map<unsigned int,ofstream*> outputs,unsigned int testUser, unsigned int testItem, map<unsigned int, bool> &firstline){
  double scoreU=0;
  double scoreI=0;
  vector<pair<unsigned int,double>>::iterator neighborVit;
  vector<unsigned int>::iterator hotVit;

  sort(trainItemsByUser[testUser].begin(), trainItemsByUser[testUser].end());
  sort(trainUsersByItem[testItem].begin(), trainUsersByItem[testItem].end());
  sort(itemsByNeighbor[testItem].begin(), itemsByNeighbor[testItem].end(),compare_pairs_first_sib);
  sort(neighborsByUser[testUser].begin(), neighborsByUser[testUser].end(),compare_pairs_first_sib);

  neighborVit = itemsByNeighbor[testItem].begin();
  hotVit = trainItemsByUser[testUser].begin();
  while (neighborVit!=itemsByNeighbor[testItem].end() && hotVit!=trainItemsByUser[testUser].end()){
    if (neighborVit->first<(*hotVit)) ++neighborVit;
    else if (*hotVit<neighborVit->first) ++hotVit;
    else {
      scoreI += neighborVit->second/itemDensity[neighborVit->first];
      ++neighborVit; ++hotVit;
    }//else
  }//while

  neighborVit = neighborsByUser[testUser].begin();
  hotVit = trainUsersByItem[testItem].begin();
  while (neighborVit!=neighborsByUser[testUser].end() && hotVit!=trainUsersByItem[testItem].end()){
    if (neighborVit->first<(*hotVit)) ++neighborVit;
    else if (*hotVit<neighborVit->first) ++hotVit;
    else {
      scoreU += (double)1/(double)userDensity[testUser];
      ++neighborVit; ++hotVit;
    }//else
  }//while

  vector<pair<double,unsigned int>>::iterator lamit;
  for(lamit=lambdas.begin(); lamit!=lambdas.end(); ++lamit){
      double score = 0;
      score = (lamit->first)*scoreU + (1.0-lamit->first)*scoreI;
      printScore((*outputs[lamit->second]),firstline[lamit->second],score,testUser,testItem,"ENSEMBLE_amau");
  }//rof lambdas
}//end of method

void Model::ENSEMBLE_aman(string outputString){
  comp_neighborsByUser({-0.5,0,-0.5,0,0});
  comp_neighborsByItem({0,-0.5,0,-0.5,1});

  vector<pair<double,unsigned int>>::iterator lamit;
  map<unsigned int,ofstream*> outputs;
  map<unsigned int, bool> firstline;
  for(lamit=lambdas.begin(); lamit!=lambdas.end(); ++lamit){
    ostringstream convert;
    convert << lamit->second;
    string str = outputString+convert.str()+".csv";
    outputs[lamit->second] = new ofstream(str);
    firstline[lamit->second] =  true;
  }

  vector<unsigned int>::iterator testUserIterator;
  unsigned int userCount = 0;
  clock_t start = clock();
  for(testUserIterator=testUsers.begin(); testUserIterator!=testUsers.end(); ++testUserIterator){
    map<unsigned int,double> scoresU;
    map<unsigned int,double> scoresI;

    vector<pair<unsigned int,double>>::iterator neighborVit;
    vector<unsigned int>::iterator testItemIterator;
    for(neighborVit=neighborsByUser[*testUserIterator].begin(); neighborVit!=neighborsByUser[*testUserIterator].end(); ++neighborVit){
      for(testItemIterator=trainItemsByUser[neighborVit->first].begin(); testItemIterator!=trainItemsByUser[neighborVit->first].end(); ++testItemIterator){
        scoresU[*testItemIterator]+= parameters[7]*(double)1/(double)userDensity[*testUserIterator];
      }//rof testItems
    }//rof user neighbors

    vector<unsigned int>::iterator hubItemIterator;
    for(hubItemIterator=trainItemsByUser[*testUserIterator].begin(); hubItemIterator!=trainItemsByUser[*testUserIterator].end(); ++hubItemIterator){
      vector<pair<unsigned int,double>>::iterator neighborVit;
      for(neighborVit=neighborsByItem[*hubItemIterator].begin(); neighborVit!=neighborsByItem[*hubItemIterator].end(); ++neighborVit){
        scoresI[neighborVit->first] += (1.0-parameters[7])*neighborVit->second/itemDensity[*hubItemIterator];
      }//rof testItems
    }//rof hubItem

    for(lamit=lambdas.begin(); lamit!=lambdas.end(); ++lamit){
      map<unsigned int,double> scores;
      map<unsigned int, double>::iterator scoreit;
      for(scoreit=scoresU.begin(); scoreit!=scoresU.end(); ++scoreit) scores[scoreit->first] += (lamit->first)*scoreit->second;
      for(scoreit=scoresI.begin(); scoreit!=scoresI.end(); ++scoreit) scores[scoreit->first] += (1.0-lamit->first)*scoreit->second;

      for(scoreit=scores.begin(); scoreit!=scores.end(); ++scoreit){
        vector<unsigned int>::iterator hubItemIterator;
        hubItemIterator = find(trainItemsByUser[*testUserIterator].begin(),trainItemsByUser[*testUserIterator].end(),scoreit->first);
        if(hubItemIterator==trainItemsByUser[*testUserIterator].end()) printScore((*outputs[lamit->second]),firstline[lamit->second],scoreit->second,*testUserIterator,scoreit->first,"ENSEMBLE_aman");
      }//rof scores
    }//rof lambdas
    ++userCount;
    printProgress("users",userCount,start);
  }//rof test users
  map<unsigned int, ofstream*>::iterator outputit;
  for(outputit=outputs.begin(); outputit!=outputs.end(); ++outputit){
    delete outputit->second;
    outputit->second = 0;
  }

}//end of method bracket


void Model::KUNN_aman(string outputString){
  comp_neighborsByUser();
  comp_itemsByNeighbor();
  ofstream output(outputString);
  bool firstline = true;

  vector<unsigned int>::iterator testUserIterator;
  unsigned int userCount = 0;
  clock_t start = clock();
  for(testUserIterator=testUsers.begin(); testUserIterator!=testUsers.end(); ++testUserIterator){
    map<unsigned int,double> scores;
    
    vector<pair<unsigned int,double>>::iterator neighborVit;
    vector<unsigned int>::iterator testItemIterator;
    for(neighborVit=neighborsByUser[*testUserIterator].begin(); neighborVit!=neighborsByUser[*testUserIterator].end(); ++neighborVit){
      for(testItemIterator=trainItemsByUser[neighborVit->first].begin(); testItemIterator!=trainItemsByUser[neighborVit->first].end(); ++testItemIterator){
        scores[*testItemIterator]+= halfWeight_KUNN("item",*testItemIterator,neighborVit->second,*testUserIterator,*testItemIterator);
      }//rof testItems
    }//rof user neighbors
    vector<unsigned int>::iterator hubItemIterator;
    for(hubItemIterator=trainItemsByUser[*testUserIterator].begin(); hubItemIterator!=trainItemsByUser[*testUserIterator].end(); ++hubItemIterator){
      vector<pair<unsigned int,double>>::iterator neighborOfVit;
      for(neighborOfVit=itemsByNeighbor[*hubItemIterator].begin(); neighborOfVit!=itemsByNeighbor[*hubItemIterator].end(); ++neighborOfVit){
        scores[neighborOfVit->first] += halfWeight_KUNN("user",*testUserIterator,neighborOfVit->second,*testUserIterator,neighborOfVit->first);
      }//rof testItems
    }//rof hubItem

    map<unsigned int, double>::iterator scoreit;
    for(scoreit=scores.begin(); scoreit!=scores.end(); ++scoreit){
      vector<unsigned int>::iterator hubItemIterator;
      hubItemIterator = find(trainItemsByUser[*testUserIterator].begin(),trainItemsByUser[*testUserIterator].end(),scoreit->first);
      if(hubItemIterator==trainItemsByUser[*testUserIterator].end()) printScore(output,firstline,scoreit->second,*testUserIterator,scoreit->first,"KUNN_aman");
    }//rof scores
    ++userCount;
    printProgress("users",userCount,start);
  }//rof test users
}//end of method bracket

void Model::KUNN_amau(string outputString){
  comp_neighborsByUser();
  comp_neighborsByItem();
  ofstream output(outputString);
  bool firstline = true;

  vector<unsigned int>::iterator testUserIterator;
  vector<unsigned int>::iterator testItemIterator;
  unsigned int linkCount = 0;
  clock_t start = clock();
  for(testUserIterator=testUsers.begin(); testUserIterator!=testUsers.end(); ++testUserIterator){
    for(testItemIterator=testItemsByUser[*testUserIterator].begin(); testItemIterator!=testItemsByUser[*testUserIterator].end(); ++testItemIterator){
      weight_KUNN(output,*testUserIterator, *testItemIterator, firstline);
      ++linkCount;
      printProgress("links",linkCount,start);
    }//for test items
  }//for test users
}//end of method

void Model::weight_KUNN(ofstream &output,unsigned int testUser, unsigned int testItem, bool &firstline){
  double score=0;
  vector<pair<unsigned int,double>>::iterator neighborVit;
  vector<unsigned int>::iterator hotVit;

  sort(trainUsersByItem[testItem].begin(), trainUsersByItem[testItem].end());
  sort(trainItemsByUser[testUser].begin(), trainItemsByUser[testUser].end());
  sort(neighborsByUser[testUser].begin(), neighborsByUser[testUser].end(),compare_pairs_first_sib);
  sort(neighborsByItem[testItem].begin(), neighborsByItem[testItem].end(),compare_pairs_first_sib);

  neighborVit = neighborsByUser[testUser].begin();
  hotVit = trainUsersByItem[testItem].begin();
  while (neighborVit!=neighborsByUser[testUser].end() && hotVit!=trainUsersByItem[testItem].end()){
    if (neighborVit->first<(*hotVit)) ++neighborVit;
    else if (*hotVit<neighborVit->first) ++hotVit;
    else {
      score += halfWeight_KUNN("item",testItem,neighborVit->second,testUser,testItem);
      ++neighborVit; ++hotVit;
    }//else
  }//while

  neighborVit = neighborsByItem[testItem].begin();
  hotVit = trainItemsByUser[testUser].begin();
  while (neighborVit!=neighborsByItem[testItem].end() && hotVit!=trainItemsByUser[testUser].end()){
    if (neighborVit->first<(*hotVit)) ++neighborVit;
    else if (*hotVit<neighborVit->first) ++hotVit;
    else {
      score += halfWeight_KUNN("user",testUser,neighborVit->second,testUser,testItem);
      ++neighborVit; ++hotVit;
    }//else
  }//while

  printScore(output,firstline,score,testUser,testItem,"KUNN_amau");
}//end of method

double Model::halfWeight_KUNN(string nodeFlavour, unsigned int testNode, double partialPathWeight,unsigned int testUser,unsigned int testItem){
  double weight = 0;
  if(nodeFlavour.compare("user")==0){
    weight = partialPathWeight*pow((double)userCount[testNode],parameters[0]);
  }
  else if(nodeFlavour.compare("item")==0){
    weight = partialPathWeight*pow((double)itemCount[testNode],parameters[1]);
  }
  return weight;
}//end of method bracket

void Model::itemBased_aman(string outputString){
  comp_neighborsByItem();
  ofstream output(outputString);
  bool firstline = true;

  vector<unsigned int>::iterator testUserIterator;
  unsigned int userCount = 0;
  clock_t start = clock();
  for(testUserIterator=testUsers.begin(); testUserIterator!=testUsers.end(); ++testUserIterator){
    map<unsigned int, double> scores;
    vector<unsigned int>::iterator hubItemIterator;
    for(hubItemIterator=trainItemsByUser[*testUserIterator].begin(); hubItemIterator!=trainItemsByUser[*testUserIterator].end(); ++hubItemIterator){
      vector<pair<unsigned int,double>>::iterator neighborVit;
      for(neighborVit=neighborsByItem[*hubItemIterator].begin(); neighborVit!=neighborsByItem[*hubItemIterator].end(); ++neighborVit){
        scores[neighborVit->first] += neighborVit->second/itemDensity[*hubItemIterator];
      }//rof testItems
    }//rof hubItem

    map<unsigned int, double>::iterator scoreit;
    for(scoreit=scores.begin(); scoreit!=scores.end(); ++scoreit){
      vector<unsigned int>::iterator hubItemIterator;
      hubItemIterator = find(trainItemsByUser[*testUserIterator].begin(),trainItemsByUser[*testUserIterator].end(),scoreit->first);
      if(hubItemIterator==trainItemsByUser[*testUserIterator].end()) printScore(output,firstline,scoreit->second,*testUserIterator,scoreit->first,"itemBased_aman");
    }//rof scores
    ++userCount;
    printProgress("users",userCount,start);
  }//rof test users
}//end of method

void Model::itemBased_amau(string outputString){
  comp_itemsByNeighbor();
  ofstream output(outputString);
  bool firstline = true;

  vector<unsigned int>::iterator testUserIterator;
  vector<unsigned int>::iterator testItemIterator;
  unsigned int linkCount = 0;
  clock_t start = clock();
  for(testUserIterator=testUsers.begin(); testUserIterator!=testUsers.end(); ++testUserIterator){
    for(testItemIterator=testItemsByUser[*testUserIterator].begin(); testItemIterator!=testItemsByUser[*testUserIterator].end(); ++testItemIterator){
      weight_itemBased(output,*testUserIterator, *testItemIterator, firstline);
      ++linkCount;
      printProgress("links",linkCount,start);
    }//for test items
  }//for test users
}//end of method

void Model::weight_itemBased(ofstream &output,unsigned int testUser, unsigned int testItem, bool &firstline){
  double score=0;
  vector<pair<unsigned int,double>>::iterator neighborVit;
  vector<unsigned int>::iterator hotVit;

  sort(trainItemsByUser[testUser].begin(), trainItemsByUser[testUser].end());
  sort(itemsByNeighbor[testItem].begin(), itemsByNeighbor[testItem].end(),compare_pairs_first_sib);

  neighborVit = itemsByNeighbor[testItem].begin();
  hotVit = trainItemsByUser[testUser].begin();
  while (neighborVit!=itemsByNeighbor[testItem].end() && hotVit!=trainItemsByUser[testUser].end()){
    if (neighborVit->first<(*hotVit)) ++neighborVit;
    else if (*hotVit<neighborVit->first) ++hotVit;
    else {
      score += neighborVit->second/itemDensity[neighborVit->first];
      ++neighborVit; ++hotVit;
    }//else
  }//while

  printScore(output,firstline,score,testUser,testItem,"itemBased_amau");
}//end of method

void Model::userBased_aman(string outputString){
  comp_neighborsByUser();
  ofstream output(outputString);
  bool firstline = true;

  vector<unsigned int>::iterator testUserIterator;
  unsigned int userCount = 0;
  clock_t start = clock();
  for(testUserIterator=testUsers.begin(); testUserIterator!=testUsers.end(); ++testUserIterator){
    map<unsigned int, double> scores;
    
    vector<pair<unsigned int,double>>::iterator neighborVit;
    vector<unsigned int>::iterator testItemIterator;
    for(neighborVit=neighborsByUser[*testUserIterator].begin(); neighborVit!=neighborsByUser[*testUserIterator].end(); ++neighborVit){
      for(testItemIterator=trainItemsByUser[neighborVit->first].begin(); testItemIterator!=trainItemsByUser[neighborVit->first].end(); ++testItemIterator){
        scores[*testItemIterator]+= (double)1/(double)userDensity[*testUserIterator];
      }//rof testItems
    }//rof user neighbors

    map<unsigned int, double>::iterator scoreit;
    for(scoreit=scores.begin(); scoreit!=scores.end(); ++scoreit){
      vector<unsigned int>::iterator hubItemIterator;
      hubItemIterator = find(trainItemsByUser[*testUserIterator].begin(),trainItemsByUser[*testUserIterator].end(),scoreit->first);
      if(hubItemIterator==trainItemsByUser[*testUserIterator].end()) printScore(output,firstline,scoreit->second,*testUserIterator,scoreit->first,"userBased_aman");
    }//rof scores
    ++userCount;
    printProgress("users",userCount,start);
  }//rof test users
}//end of method

void Model::userBased_amau(string outputString){
  comp_neighborsByUser();
  ofstream output(outputString);
  bool firstline = true;

  vector<unsigned int>::iterator testUserIterator;
  vector<unsigned int>::iterator testItemIterator;
  unsigned int linkCount = 0;
  clock_t start = clock();
  for(testUserIterator=testUsers.begin(); testUserIterator!=testUsers.end(); ++testUserIterator){
    for(testItemIterator=testItemsByUser[*testUserIterator].begin(); testItemIterator!=testItemsByUser[*testUserIterator].end(); ++testItemIterator){
      weight_userBased(output,*testUserIterator, *testItemIterator, firstline);
      ++linkCount;
      printProgress("links",linkCount,start);
    }//for test items
  }//for test users
}//end of method

void Model::weight_userBased(ofstream &output,unsigned int testUser, unsigned int testItem, bool &firstline){
  double score=0;
  vector<pair<unsigned int,double>>::iterator neighborVit;
  vector<unsigned int>::iterator hotVit;

  sort(trainUsersByItem[testItem].begin(), trainUsersByItem[testItem].end());
  sort(neighborsByUser[testUser].begin(), neighborsByUser[testUser].end(),compare_pairs_first_sib);

  neighborVit = neighborsByUser[testUser].begin();
  hotVit = trainUsersByItem[testItem].begin();
  while (neighborVit!=neighborsByUser[testUser].end() && hotVit!=trainUsersByItem[testItem].end()){
    if (neighborVit->first<(*hotVit)) ++neighborVit;
    else if (*hotVit<neighborVit->first) ++hotVit;
    else {
      score += (double)1/(double)userDensity[testUser];
      ++neighborVit; ++hotVit;
    }//else
  }//while

  printScore(output,firstline,score,testUser,testItem,"userBased_amau");
}//end of method

void Model::comp_neighborsByUser(){
  cout << "start calc neighbors by user" << endl << flush;
  vector<unsigned int>::iterator targetVit;
  vector<unsigned int>::iterator hubVit;
  vector<unsigned int>::iterator neighborVit;
  clock_t start;
  unsigned int targetsDone;

  start = clock();
  targetsDone=0;
  for(targetVit=testUsers.begin(); targetVit!=testUsers.end(); ++targetVit){
    map<unsigned int,double> scores;
    for(hubVit=trainItemsByUser[*targetVit].begin(); hubVit!=trainItemsByUser[*targetVit].end(); ++hubVit){
      for(neighborVit=trainUsersByItem[*hubVit].begin(); neighborVit!=trainUsersByItem[*hubVit].end(); ++neighborVit){
        scores[*neighborVit] += pow((double)userCount[*targetVit],parameters[0])*pow((double)userCount[*neighborVit],parameters[2])*pow((double)itemCount[*hubVit],parameters[3]);
      }//neighbors for
    }//hubs for
    scores.erase(*targetVit);//remove target itself from map
    vector<pair<unsigned int,double>> neighbors;
    copy(scores.begin(), scores.end(), back_inserter(neighbors));
    if(neighbors.size()>0 && parameters[5]>0){
      unsigned int n = max(min(neighbors.size(),(size_t)parameters[5]),(size_t)1)-1;
      nth_element(neighbors.begin(),neighbors.begin()+n,neighbors.end(),compare_pairs_second_bib);
      double kval = neighbors[n].second;
      vector<pair<unsigned int,double>>::iterator nit;
      vector<pair<unsigned int,double>>::iterator swit = neighbors.begin()+n+1;
      for(nit=neighbors.begin(); nit!=swit; ++nit) neighborsByUser[*targetVit].push_back(*nit), userDensity[*targetVit]+=pow(nit->second,parameters[4]);
      for(nit=swit; nit!=neighbors.end(); ++nit) if(nit->second==kval) neighborsByUser[*targetVit].push_back(*nit), userDensity[*targetVit]+=pow(nit->second,parameters[4]);
      // if(neighborsByUser[*configVit][*targetVit].size()<(noUserNodes-1)) cout << "neighbors = " << neighborsByUser[*configVit][*targetVit].size() << " i.s.o. " << noUserNodes-1<<endl << flush;
    }//if

    ++targetsDone;
    printProgress("test-user-neighbors",targetsDone,start);
  }//target for

  cout << "after calculating neighbors by user" << endl << flush;

}//end of method bracket

void Model::comp_neighborsByUser(vector<double> altParameters){
  cout << "start calc neighbors by user" << endl << flush;
  vector<unsigned int>::iterator targetVit;
  vector<unsigned int>::iterator hubVit;
  vector<unsigned int>::iterator neighborVit;
  clock_t start;
  unsigned int targetsDone;

  start = clock();
  targetsDone=0;
  for(targetVit=testUsers.begin(); targetVit!=testUsers.end(); ++targetVit){
    map<unsigned int,double> scores;
    for(hubVit=trainItemsByUser[*targetVit].begin(); hubVit!=trainItemsByUser[*targetVit].end(); ++hubVit){
      for(neighborVit=trainUsersByItem[*hubVit].begin(); neighborVit!=trainUsersByItem[*hubVit].end(); ++neighborVit){
        scores[*neighborVit] += pow((double)userCount[*targetVit],altParameters[0])*pow((double)userCount[*neighborVit],altParameters[2])*pow((double)itemCount[*hubVit],altParameters[3]);
      }//neighbors for
    }//hubs for
    scores.erase(*targetVit);//remove target itself from map
    vector<pair<unsigned int,double>> neighbors;
    copy(scores.begin(), scores.end(), back_inserter(neighbors));
    if(neighbors.size()>0 && parameters[5]>0){
      unsigned int n = max(min(neighbors.size(),(size_t)parameters[5]),(size_t)1)-1;
      nth_element(neighbors.begin(),neighbors.begin()+n,neighbors.end(),compare_pairs_second_bib);
      double kval = neighbors[n].second;
      vector<pair<unsigned int,double>>::iterator nit;
      vector<pair<unsigned int,double>>::iterator swit = neighbors.begin()+n+1;
      for(nit=neighbors.begin(); nit!=swit; ++nit) neighborsByUser[*targetVit].push_back(*nit), userDensity[*targetVit]+=pow(nit->second,altParameters[4]);
      for(nit=swit; nit!=neighbors.end(); ++nit) if(nit->second==kval) neighborsByUser[*targetVit].push_back(*nit), userDensity[*targetVit]+=pow(nit->second,altParameters[4]);
      // if(neighborsByUser[*configVit][*targetVit].size()<(noUserNodes-1)) cout << "neighbors = " << neighborsByUser[*configVit][*targetVit].size() << " i.s.o. " << noUserNodes-1<<endl << flush;
    }//if

    ++targetsDone;
    printProgress("test-user-neighbors",targetsDone,start);
  }//target for

  cout << "after calculating neighbors by user" << endl << flush;

}//end of method bracket

void Model::comp_neighborsByItem(){
  cout << "start calc neighbors by item" << endl << flush;
  vector<unsigned int>::iterator targetVit;
  vector<unsigned int>::iterator hubVit;
  vector<unsigned int>::iterator neighborVit;
  clock_t start;
  unsigned int targetsDone;

  start = clock();
  targetsDone=0;
  for(targetVit=testItems.begin(); targetVit!=testItems.end(); ++targetVit){
    map<unsigned int,double> scores;
    // cout << "test item: " << *targetVit << endl << flush;
    for(hubVit=trainUsersByItem[*targetVit].begin(); hubVit!=trainUsersByItem[*targetVit].end(); ++hubVit){
      // cout << "hub user: " << *hubVit << endl << flush;
      for(neighborVit=trainItemsByUser[*hubVit].begin(); neighborVit!=trainItemsByUser[*hubVit].end(); ++neighborVit){
        // cout << "neighbor item: " << *neighborVit << "for test item: " << *targetVit << endl << flush;
        scores[*neighborVit] += pow((double)itemCount[*targetVit],parameters[1])*pow((double)itemCount[*neighborVit],parameters[3])*pow((double)userCount[*hubVit],parameters[2]);
      }//neighbors for
    }//hubs for
    scores.erase(*targetVit);//remove target itself from map
    vector<pair<unsigned int,double>> neighbors;
    copy(scores.begin(), scores.end(), back_inserter(neighbors));
    // cout << "size of neighbors: " << neighbors.size() << endl << flush;
    if(neighbors.size()>0 && parameters[6]>0){
      unsigned int n = max(min(neighbors.size(),(size_t)parameters[6]),(size_t)1)-1;
      nth_element(neighbors.begin(),neighbors.begin()+n,neighbors.end(),compare_pairs_second_bib);
      double kval = neighbors[n].second;
      vector<pair<unsigned int,double>>::iterator nit;
      vector<pair<unsigned int,double>>::iterator swit = neighbors.begin()+n+1;
      for(nit=neighbors.begin(); nit!=swit; ++nit) neighborsByItem[*targetVit].push_back(*nit), itemDensity[*targetVit]+=pow(nit->second,parameters[4]);
      for(nit=swit; nit!=neighbors.end(); ++nit) if(nit->second==kval) neighborsByItem[*targetVit].push_back(*nit), itemDensity[*targetVit]+=pow(nit->second,parameters[4]);
      // if(neighborsByItem[*configVit][*targetVit].size()<(noItemNodes-1)) cout << "neighbors = " << neighborsByItem[*configVit][*targetVit].size() << " i.s.o. " << noItemNodes-1<<endl << flush;
    }//fi
    ++targetsDone;
    printProgress("test-item-neighbors",targetsDone,start);
  }//target for

  cout << "after calculating neighbors by item" << endl << flush;
}//end of method

void Model::comp_neighborsByItem(vector<double> altParameters){
  cout << "start calc neighbors by item" << endl << flush;
  vector<unsigned int>::iterator targetVit;
  vector<unsigned int>::iterator hubVit;
  vector<unsigned int>::iterator neighborVit;
  clock_t start;
  unsigned int targetsDone;

  start = clock();
  targetsDone=0;
  for(targetVit=testItems.begin(); targetVit!=testItems.end(); ++targetVit){
    map<unsigned int,double> scores;
    // cout << "test item: " << *targetVit << endl << flush;
    for(hubVit=trainUsersByItem[*targetVit].begin(); hubVit!=trainUsersByItem[*targetVit].end(); ++hubVit){
      // cout << "hub user: " << *hubVit << endl << flush;
      for(neighborVit=trainItemsByUser[*hubVit].begin(); neighborVit!=trainItemsByUser[*hubVit].end(); ++neighborVit){
        // cout << "neighbor item: " << *neighborVit << "for test item: " << *targetVit << endl << flush;
        scores[*neighborVit] += pow((double)itemCount[*targetVit],altParameters[1])*pow((double)itemCount[*neighborVit],altParameters[3])*pow((double)userCount[*hubVit],altParameters[2]);
      }//neighbors for
    }//hubs for
    scores.erase(*targetVit);//remove target itself from map
    vector<pair<unsigned int,double>> neighbors;
    copy(scores.begin(), scores.end(), back_inserter(neighbors));
    // cout << "size of neighbors: " << neighbors.size() << endl << flush;
    if(neighbors.size()>0 && parameters[6]>0){
      unsigned int n = max(min(neighbors.size(),(size_t)parameters[6]),(size_t)1)-1;
      nth_element(neighbors.begin(),neighbors.begin()+n,neighbors.end(),compare_pairs_second_bib);
      double kval = neighbors[n].second;
      vector<pair<unsigned int,double>>::iterator nit;
      vector<pair<unsigned int,double>>::iterator swit = neighbors.begin()+n+1;
      for(nit=neighbors.begin(); nit!=swit; ++nit) neighborsByItem[*targetVit].push_back(*nit), itemDensity[*targetVit]+=pow(nit->second,altParameters[4]);
      for(nit=swit; nit!=neighbors.end(); ++nit) if(nit->second==kval) neighborsByItem[*targetVit].push_back(*nit), itemDensity[*targetVit]+=pow(nit->second,altParameters[4]);
      // if(neighborsByItem[*configVit][*targetVit].size()<(noItemNodes-1)) cout << "neighbors = " << neighborsByItem[*configVit][*targetVit].size() << " i.s.o. " << noItemNodes-1<<endl << flush;
    }//fi
    ++targetsDone;
    printProgress("test-item-neighbors",targetsDone,start);
  }//target for

  cout << "after calculating neighbors by item" << endl << flush;
}//end of method

void Model::comp_itemsByNeighbor(){
  cout << "start calc items by neighbor" << endl << flush;
  vector<unsigned int>::iterator targetVit;
  vector<unsigned int>::iterator hubVit;
  vector<unsigned int>::iterator neighborVit;
  clock_t start;
  unsigned int targetsDone;
  start = clock();
  targetsDone=0;
  for(targetVit=testItems.begin(); targetVit!=testItems.end(); ++targetVit){
    map<unsigned int,double> scores;
    // cout << "test item: " << *targetVit << endl << flush;
    for(hubVit=trainUsersByItem[*targetVit].begin(); hubVit!=trainUsersByItem[*targetVit].end(); ++hubVit){
      // cout << "hub user: " << *hubVit << endl << flush;
      for(neighborVit=trainItemsByUser[*hubVit].begin(); neighborVit!=trainItemsByUser[*hubVit].end(); ++neighborVit){
        // cout << "neighbor item: " << *neighborVit << "for test item: " << *targetVit << endl << flush;    
        scores[*neighborVit] += pow((double)itemCount[*targetVit],parameters[1])*pow((double)itemCount[*neighborVit],parameters[3])*pow((double)userCount[*hubVit],parameters[2]);
      }//neighbors for
    }//hubs for
    scores.erase(*targetVit);//remove target itself from map
    vector<pair<unsigned int,double>> neighbors;
    copy(scores.begin(), scores.end(), back_inserter(neighbors));
    // cout << "size of neighbors: " << neighbors.size() << endl << flush;
    if(neighbors.size()>0 && parameters[6]>0){
      unsigned int n = max(min(neighbors.size(),(size_t)parameters[6]),(size_t)1)-1;
      nth_element(neighbors.begin(),neighbors.begin()+n,neighbors.end(),compare_pairs_second_bib);
      double kval = neighbors[n].second;
      vector<pair<unsigned int,double>>::iterator nit;
      vector<pair<unsigned int,double>>::iterator swit = neighbors.begin()+n+1;
      for(nit=neighbors.begin(); nit!=swit; ++nit) itemsByNeighbor[nit->first].push_back(make_pair(*targetVit,nit->second)), itemDensity[*targetVit]+=pow(nit->second,parameters[4]);
      for(nit=swit; nit!=neighbors.end(); ++nit) if(nit->second==kval) itemsByNeighbor[nit->first].push_back(make_pair(*targetVit,nit->second)), itemDensity[*targetVit]+=pow(nit->second,parameters[4]);
      // if(neighborsByItem[*configVit][*targetVit].size()<(noItemNodes-1)) cout << "neighbors = " << neighborsByItem[*configVit][*targetVit].size() << " i.s.o. " << noItemNodes-1<<endl << flush;
    }//fi
    ++targetsDone;
    printProgress("test-item-neighbors",targetsDone,start);
  }//target for
  cout << "finished calculation items by neighbor" << endl << flush;
}//end of method

void Model::comp_itemsByNeighbor(vector<double> altParameters){
  cout << "start calc items by neighbor" << endl << flush;
  vector<unsigned int>::iterator targetVit;
  vector<unsigned int>::iterator hubVit;
  vector<unsigned int>::iterator neighborVit;
  clock_t start;
  unsigned int targetsDone;

  start = clock();
  targetsDone=0;
  for(targetVit=testItems.begin(); targetVit!=testItems.end(); ++targetVit){
    map<unsigned int,double> scores;
    // cout << "test item: " << *targetVit << endl << flush;
    for(hubVit=trainUsersByItem[*targetVit].begin(); hubVit!=trainUsersByItem[*targetVit].end(); ++hubVit){
      // cout << "hub user: " << *hubVit << endl << flush;
      for(neighborVit=trainItemsByUser[*hubVit].begin(); neighborVit!=trainItemsByUser[*hubVit].end(); ++neighborVit){
        // cout << "neighbor item: " << *neighborVit << "for test item: " << *targetVit << endl << flush;    
        scores[*neighborVit] += pow((double)itemCount[*targetVit],altParameters[1])*pow((double)itemCount[*neighborVit],altParameters[3])*pow((double)userCount[*hubVit],altParameters[2]);
      }//neighbors for
    }//hubs for
    scores.erase(*targetVit);//remove target itself from map
    vector<pair<unsigned int,double>> neighbors;
    copy(scores.begin(), scores.end(), back_inserter(neighbors));
    // cout << "size of neighbors: " << neighbors.size() << endl << flush;
    if(neighbors.size()>0 && parameters[6]>0){
      unsigned int n = max(min(neighbors.size(),(size_t)parameters[6]),(size_t)1)-1;
      nth_element(neighbors.begin(),neighbors.begin()+n,neighbors.end(),compare_pairs_second_bib);
      double kval = neighbors[n].second;
      vector<pair<unsigned int,double>>::iterator nit;
      vector<pair<unsigned int,double>>::iterator swit = neighbors.begin()+n+1;
      for(nit=neighbors.begin(); nit!=swit; ++nit) itemsByNeighbor[nit->first].push_back(make_pair(*targetVit,nit->second)), itemDensity[*targetVit]+=pow(nit->second,altParameters[4]);
      for(nit=swit; nit!=neighbors.end(); ++nit) if(nit->second==kval) itemsByNeighbor[nit->first].push_back(make_pair(*targetVit,nit->second)), itemDensity[*targetVit]+=pow(nit->second,altParameters[4]);
      // if(neighborsByItem[*configVit][*targetVit].size()<(noItemNodes-1)) cout << "neighbors = " << neighborsByItem[*configVit][*targetVit].size() << " i.s.o. " << noItemNodes-1<<endl << flush;
    }//fi
    ++targetsDone;
    printProgress("test-item-neighbors",targetsDone,start);
  }//target for
  cout << "finished calculation items by neighbor" << endl << flush;
}//end of method

void Model::printScore(ofstream &output, bool &firstline, double &score, unsigned int user, unsigned int item, string method){
  //print headers only ones
  if(firstline){
    output << "user," << "item," << method << "," << endl << flush; 
    firstline=false;
  }
  //print score
  output << user << "," << item << "," << score << "," << endl << flush;
}//end of method

void Model::printProgress(string entity, unsigned int linkCount, clock_t start){
  if (linkCount == 2) {
      cout << "time for 2 "<< entity << " : " << "\t[" << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl << flush;
    } else if (linkCount == 100) {
        cout << "time for 100 "<< entity << " : " << "\t[" << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl << flush;
      } else if (linkCount == 1000) {
          cout << "time for 1000 "<< entity << " : " << "\t[" << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl << flush;
        } else if (linkCount == 10000){
            cout << "time for 10 000 "<< entity << " : " << "\t[" << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl << flush;
          }
}//end of method



