#Summary
This readme file gives instructions on how to reproduce the expediments reported in the paper 

[Unifying Nearest Neighbors Collaborative Filtering](http://adrem.ua.ac.be/bibrem/pubs/verstrepen14kunn.pdf), In Proceedings of the 8th ACM Conference on Recommender Systems (RecSys'14), 2014. 

Furthermore this file contains some pointers towards some additional materials. We discuss the following steps:

1. Additional materials
2. Clone this repository to your machine
3. Complie the Nearest Neighbors Algorithms
4. Install the MyMediaLite library for running the Matrix Factorization algorithms
5. Get the data
6. Get the datasets in the correct format
7. Run the scripts to repeat the experiments

#1. Additional materials
The file **addendum_on_parameters.pdf** is for readers who do not want to reproduce the experiments but only want to gain insight in the parammeters found by the grid search procedure.
Furthermore, this addendum shows how KUNN is relatively insensitive to its paraemters.

#2. Clone this repository to your machine
Click on the **clone** button and follow the instructions. All subsequent actions will assume the cloned folder _~/unncf_submit/_ as root. We will refer to this folder as the working directory. 

#3. Compile the Nearest Neighbors Algorithms
The source-code for the nearest neighbours algorithms, including KUNN, can be found in the folder _/unn/_ of this repository. Navigate to this folder.

```
cd unn
```

Compile the source code.

```
make
```

We compiled the source with gcc version 4.6.3 (Ubuntu/Linaro 4.6.3-1ubuntu5) on Ubuntu 12.04.4 LTS

Return to the working directory.

```
cd ..
```

#4. Install the MyMediaLite library for running the Matrix Factorization algorithms
For the matrix factorisation algorithms, we used the implementations in the MyMediaLite library. The library can be downloaded at the [MyMediaLite website](http://mymedialite.net/download/index.html). 
This website also contains installation instructions. The scripts discussed in Section 7 take care of running the algorithms, don't bother.

#5. Get the data

All datasets are publicly available, readily or by request to the owner. They can be found at the following locations:

* Movielens1M at [http://grouplens.org/datasets/movielens/](http://grouplens.org/datasets/movielens/) 
* Yahoo!Music at [http://webscope.sandbox.yahoo.com](http://webscope.sandbox.yahoo.com) 

#6. Get the dataset in the correct format
* unpack all compressed containers
* For Movielens, copy _movielens1M.data_ to the working directory
* For Yahoo!Music, 
    * rename _ydata-ymusic-rating-study-v1_0-train.txt_ to _yahoo_train.txt_ and copy it to the working directory
    * rename _ydata-ymusic-rating-study-v1_0-test.txt_ to _yahoo_test.txt_ and copy it to the working directory
	
#7. Run the scripts to repeat the experiments
All scripts for running experiments and generating the numbers in Table 3 of the paper have a filename with the prefix **run_kdd_experiment***.
The suffix of the filenames corresponds to the dataset and the algorithm.

The algorithms are splitted in three groups. The experiment scripts contain one of the following three algorithm indicators in their suffix:

* **NN**: nearest neighbors algorithms being user-based, item-based and KUNN (user-based-cosine, item-based-cosine-SNorm+, KUNN)
* **ENSEMBLE**: linear ensemble of user-based and item-based nearest neighbors (UB+IB)
* **MF**: matrix factorization algorithms (WRMF, BPRMF)

The experiment scripts also contain one of the three dataset indicators in their suffix:

* **aman_movielens**: Movielens
* **aman_yahoo**: Yahoo!Music-_user_
* **amau_yahoo**: Yahoo!Music-_random_

Finally, all results of the baseline algorithm **pop** are generated with the files

* pop_movielens.py and
* pop_yahoo.py

on the respective datasets.

Before running one of the scripts, set the number of nodes available for the experiments in on of the top lines of the file. For example, when 10 nodes are available:

```python
noWorkerNodes = 10
```
We ran the experiments on a machine with 32 nodes and 32G memory.

After specifying the number of nodes, you can for example start the experiments for the nearest neighbors algorithms on the Yahoo!Music-_random_ dataset as

```
python run_kdd_experiment_NN_amau_yahoo.py 
```

A similar way of working applies to every other **run_kdd_experiment**-file.


