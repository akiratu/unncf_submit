'''
Last update: Feb 2014
koen.verstrepen@gmail.com

Performs experiments with Nearest Neighbors algorithms for KDD2014 submission
'''
from collections import defaultdict
import numpy as np
import operator
import random
import subprocess
import math
import os
import shutil
import time
import multiprocessing
from eval_kdd import eval_kdd
################################################################################################
# MAIN
################################################################################################
def main():
	oFolds = [1,2,3,4,5]
	noFolds = len(oFolds)
	delimiter = '\t'
	dataSet = 'yahoo'
	rawDataString = './yahoo_train.txt'
	rawTestString = './yahoo_test.txt'
	smallestPos = 4
	largestNeg = 2
	nHoldOut = 1
	expFolder = './exp_kdd_pop_'+dataSet+'_x'+str(noFolds)+'_sp'+str(smallestPos)+'_ln'+str(largestNeg)+'/'
	if not os.path.exists(expFolder):
		os.makedirs(expFolder)
	logString = expFolder+'log.txt'
	noWorkerNodes = 1
	amauFolder = expFolder
	amanFolder = expFolder

	#read raw data
	users = set()
	items = set()
	itemCount = defaultdict(int)
	itemsByUser = defaultdict(set)
	negsByUser = defaultdict(set)
	with open(rawDataString,'r') as rawData:
		for line in rawData:
			splitted = line.replace('::',' ').split()
			user = int(splitted[0])
			item = int(splitted[1])
			rating = int(splitted[2])
			items.add(item)
			if rating >= smallestPos:
				itemCount[item]+=1
				itemsByUser[user].add(item)
				users.add(user)
			if rating <= largestNeg:
				negsByUser[user].add(item)
	noItems = len(items)

	#score items
	itemScores = [(item,itemCount[item]) for item in itemCount]
	itemScores.sort(key=lambda tup: tup[1], reverse=True)
	scoreByItem = {}
	remainingItems = set(items)
	for i,tup in enumerate(itemScores):
		item = tup[0]
		scoreByItem[item] = noItems-i
		remainingItems.remove(item)
	remainingItems = list(remainingItems)
	random.shuffle(remainingItems)
	for j,item in enumerate(remainingItems):
		i = len(itemScores)+j
		scoreByItem[item] = noItems-i

	#create data splits en score files
	for oFold in oFolds:
		#oFold determines randomization
		random.seed(oFold)

		if not os.path.exists(amanFolder+str(oFold)+'/pop'):
			os.makedirs(amanFolder+str(oFold)+'/pop')

		#define file paths
		trainString = amanFolder+str(oFold)+'/train.tsv'
		testString = amanFolder+str(oFold)+'/test.tsv'
		groundTruthString = amanFolder+str(oFold)+'/ground_truth.tsv'
		scoreString = amanFolder+str(oFold)+'/pop/scores_nr1.csv'
		with open(scoreString,'a') as scoreFile:
			scoreFile.write('user,item,pop,\n')

		for user in users:
			# write files for oFold
			remaining = split_user_aman(itemsByUser[user],nHoldOut,trainString,testString,groundTruthString,user,delimiter,scoreString,scoreByItem,items)

	for oFold in oFolds:
		print 'start eval for fold %s' % oFold
		#evaluate aman
		eval_kdd(amanFolder+str(oFold)+'/','pop',1,'aman',{})

	#summarize
	results = defaultdict(lambda: defaultdict(list))
	for folder in [amauFolder,amanFolder]:
		for oFold in oFolds:
			sourceFolder = folder+str(oFold)+'/pop/'
			for filename in os.listdir(sourceFolder):
				if filename.startswith('results'):
					with open(sourceFolder+filename,'r') as sourceFile:
						headerLine = sourceFile.readline()
						for line in sourceFile:
							splitted = line.split(',')
							method = splitted[1]
							metr = splitted[2]
							score = float(splitted[3])
							results[method][metr].append(score)
	summaryString = expFolder+'restults.csv'
	with open(summaryString,'a') as summaryFile:
		summaryFile.write('algorithm,metric,av,std,scores\n')
		for method in results.keys():
			for metric in results[method].keys():
				scoreArray = np.array(results[method][metric])
				av = np.mean(scoreArray)
				std = np.std(scoreArray)
				summaryFile.write('%s,%s,%s,%s,' % (method,metric,av,std))
				for score in results[method][metric]:
					summaryFile.write('%s,' % score)
				summaryFile.write(' \n')

	#amau
	#create final train, test and ground truth data
	trainString = expFolder+'train.tsv'
	with open(trainString,'w') as trainFile:
		for user in itemsByUser:
			for item in itemsByUser[user]:
				trainFile.write(str(user)+delimiter+str(item)+delimiter+'5'+'\n')
	testString = expFolder+'test.tsv'
	groundTruthString = expFolder+'ground_truth.tsv'
	testcouples = set()
	with open(rawTestString,'r') as rawTestFile:
		with open(testString,'w') as testFile:
			with open(groundTruthString,'w') as gtFile:
				for line in rawTestFile:
					splitted = line.split()
					user = int(splitted[0])
					item = int(splitted[1])
					testcouples.add((user,item))
					rating = int(splitted[2])
					if rating >= smallestPos:
						gtFile.write(str(user)+delimiter+str(item)+delimiter+'1'+'\n')
						testFile.write(str(user)+delimiter+str(item)+delimiter+'0'+'\n')
					if rating <= largestNeg:
						gtFile.write(str(user)+delimiter+str(item)+delimiter+'-1'+'\n')
						testFile.write(str(user)+delimiter+str(item)+delimiter+'0'+'\n')
	#create score file
	scoreFolder = expFolder+'pop/'
	if not os.path.exists(scoreFolder):
		os.makedirs(scoreFolder)
	scoreString = scoreFolder+'scores_nr1.csv'
	with open(scoreString,'a') as scoreFile:
		scoreFile.write('user,item,pop, \n')
		for user,item in testcouples:
			scoreFile.write('%s,%s,%s, \n' % (user,item,scoreByItem[item])) 


	#eval
	eval_kdd(expFolder,'pop',1,'auc_amau_users',{})

	#add to summary
	results = defaultdict(lambda: defaultdict(list))
	sourceString = scoreFolder+'results_auc_amau_users_nr1.csv'
	with open(sourceString,'r') as sourceFile:
		headerline = sourceFile.readline()
		secondLine = sourceFile.readline()
		splitted = secondLine.split(',')
		method = splitted[1]
		metr = splitted[2]
		score = float(splitted[3])
		with open(summaryString,'a') as summaryFile:
			summaryFile.write('%s,%s,%s,\n' % (method,metr,score))





	return
################################################################################################
# FUNCTIONS
################################################################################################
def split_user_aman(itemsByUser,nHoldOut,trainString,testString,groundTruthString,user,delimiter,scoreString,scoreByItem,items):
	testItems = set()
	trainItems = set()
	inTest = False
	if len(itemsByUser)>(nHoldOut):
		candidates = list(itemsByUser)
		random.shuffle(candidates)
		testItems = set(candidates[0:nHoldOut])
		trainItems = set(candidates[nHoldOut:])
		inTest = True
	elif len(itemsByUser)>1:
		candidates = list(itemsByUser)
		random.shuffle(candidates)
		testItems = set(candidates[0:-1])
		trainItems = set(candidates[-1])
		inTest = True
	if inTest:
		with open(testString,'a') as testFile:
			testFile.write(str(user)+delimiter+'dummy'+'\n')
		with open(groundTruthString,'a') as gtFile:
			for testItem in testItems:
				gtFile.write(str(user)+delimiter+str(testItem)+delimiter+str(1)+'\n')
		with open(scoreString,'a') as scoreFile:
			for item in items:
				scoreFile.write(str(user)+','+str(item)+','+str(scoreByItem[item])+', \n')
	with open(trainString,'a') as trainFile:
		for item in trainItems:
			trainFile.write(str(user)+delimiter+str(item)+delimiter+str(5)+'\n')
	return trainItems

def split_user_amau(negsByUser,itemsByUser,nHoldOut,trainString,testString,groundTruthString,user,delimiter,scoreString,scoreByItem):
	testItems = set()
	trainItems = set()
	inTest = False
	# choose positive testitem
	if len(itemsByUser)>(nHoldOut):
		candidates = list(itemsByUser)
		random.shuffle(candidates)
		for candidate in candidates[0:nHoldOut]:
			testItems.add((candidate,1))
		trainItems = set(candidates[nHoldOut:])
		inTest = True
	elif len(itemsByUser)>1:
		candidates = list(itemsByUser)
		random.shuffle(candidates)
		for candidate in candidates[0:-1]:
			testItems.add((candidate,1))
		trainItems = set(candidates[-1])
		inTest = True
	#write files
	if inTest:
		#add negs to test items
		for neg in negsByUser:
			testItems.add((neg,-1))
		with open(testString,'a') as testFile:
			for testItem in testItems:
				testFile.write(str(user)+delimiter+str(testItem[0])+delimiter+'0'+'\n')
		with open(groundTruthString,'a') as gtFile:
			for testItem in testItems:
				gtFile.write(str(user)+delimiter+str(testItem[0])+delimiter+str(testItem[1])+'\n')
		with open(scoreString,'a') as scoreFile:
			for testItem in testItems:
				scoreFile.write(str(user)+','+str(testItem[0])+','+str(scoreByItem[testItem[0]])+', \n')
	with open(trainString,'a') as trainFile:
		for item in trainItems:
			trainFile.write(str(user)+delimiter+str(item)+delimiter+str(5)+'\n')
	return trainItems

################################################################################################
# EXECUTE
################################################################################################
if __name__ == '__main__':
	main()