'''
Last update: Feb 2014
koen.verstrepen@gmail.com

Performs experiments with MyMediaLite MF algorithms for KDD2014 submission
'''
from collections import defaultdict
import numpy as np
import operator
import random
import subprocess
import math
import os
import time
import multiprocessing
from eval_kdd import eval_kdd
################################################################################################
# MAIN
################################################################################################
def main():
	#initializaton
	oFolds = [1,2,3,4,5]
	noFolds = len(oFolds)
	iFolds = [1,2,3,4,5]
	niFolds = len(iFolds)
	algorithms = ['WRMF','BPRMF']
	delimiter = '\t'
	dataSet = 'yahoo'
	rawDataString = './yahoo_train.txt'
	rawTestString = './yahoo_test.txt'
	smallestPos = 4
	largestNeg = 2
	nHoldOut = 1
	nMinTrain = 1
	expFolder = './exp_kdd_MF_amau_'+dataSet+'_x'+str(noFolds)+'_sp'+str(smallestPos)+'_ln'+str(largestNeg)+'/'
	if not os.path.exists(expFolder):
		os.makedirs(expFolder)
	logString = expFolder+'log.txt'
	noWorkerNodes = 25

	#read raw data
	users = set()
	items = set()
	itemsByUser = defaultdict(set)
	negsByUser =defaultdict(set)
	with open(rawDataString,'r') as rawData:
		for line in rawData:
			splitted = line.replace('::',' ').split()
			user = int(splitted[0])
			item = int(splitted[1])
			rating = int(splitted[2])
			users.add(user)
			items.add(item)
			if rating >= smallestPos:
				itemsByUser[user].add(item)
			if rating <= largestNeg:
				negsByUser[user].add(item)

	#create data splits for determining parameters
	for user in users:
		for oFold in oFolds:
			if not os.path.exists(expFolder+str(oFold)+'/'):
				os.makedirs(expFolder+str(oFold)+'/')
			#oFold determines randomization
			random.seed(oFold)
			#define file paths
			trainString = expFolder+str(oFold)+'/train.tsv'
			testString = expFolder+str(oFold)+'/test.tsv'
			groundTruthString = expFolder+str(oFold)+'/ground_truth.tsv'
			# write files for oFold
			remaining = split_user(negsByUser[user],itemsByUser[user],nHoldOut,trainString,testString,groundTruthString,user,delimiter)


	#define processes
	inputQ = multiprocessing.JoinableQueue()
	#for outerfolds
	pid = 0
	for oFold in oFolds:
		for algorithm in algorithms:
			#parameters combinations are based on default values, reported values and experience
			#for parametersets
			if algorithm == 'WRMF':
				num_iter = 15
				for a in [10, 40, 60, 100, 1000]:
					for regularization in [0.1,1,10,100,1000]:
						for num_factors in [10,20,30]:
							pid+=1
							inputQ.put((str(oFold)+'/','',algorithm,{'a':a,'regularization':regularization,'num_factors':num_factors,'num_iter':num_iter},'auc_amau_users',pid))
			elif algorithm == 'BPRMF':
				for bias_reg in [0, 1]:
					for reg_u in [0.00025, 0.0025]:
						for reg_i in [0.00025, 0.0025]:
							for reg_j in [0.00025, 0.0025]:
								for num_factors in [10,20,30]:
									for num_iter in [30]:
										for learn_rate in [0.0001, 0.001, 0.01, 0.1]:
											pid+=1
											inputQ.put((str(oFold)+'/','',algorithm,{'bias_reg':bias_reg,'reg_u':reg_u,'reg_i':reg_i,'reg_j':reg_j,'num_factors':num_factors,'num_iter':num_iter,'learn_rate':learn_rate},'auc_amau_users',pid))

	#add poison pills
	for i in xrange(noWorkerNodes):
		inputQ.put(None)
		print 'added poison pill %s out of %s' % (str(i+1),str(noWorkerNodes))

	#start processes
	for i in xrange(noWorkerNodes):
		p = multiprocessing.Process(target=score_worker, args=(inputQ,expFolder))
		p.start()
		print 'started process %s of %s' % (str(i+1),str(noWorkerNodes))

	#wait for all processes to join
	print 'now waiting for all processes to join inputQ'
	inputQ.join()
	print 'joined inputQ, start evaluation'

	#create final train, test and ground truth data
	trainString = expFolder+'train.tsv'
	with open(trainString,'w') as trainFile:
		for user in itemsByUser:
			for item in itemsByUser[user]:
				trainFile.write(str(user)+delimiter+str(item)+delimiter+'5'+'\n')
	testString = expFolder+'test.tsv'
	groundTruthString = expFolder+'ground_truth.tsv'
	with open(rawTestString,'r') as rawTestFile:
		with open(testString,'w') as testFile:
			with open(groundTruthString,'w') as gtFile:
				for line in rawTestFile:
					splitted = line.split()
					user = int(splitted[0])
					item = int(splitted[1])
					rating = int(splitted[2])
					if rating >= smallestPos:
						gtFile.write(str(user)+delimiter+str(item)+delimiter+'1'+'\n')
						testFile.write(str(user)+delimiter+str(item)+delimiter+'0'+'\n')
					if rating <= largestNeg:
						gtFile.write(str(user)+delimiter+str(item)+delimiter+'-1'+'\n')
						testFile.write(str(user)+delimiter+str(item)+delimiter+'0'+'\n')

	#define inputs for each (algorithm,metric,outerfold) tuple
	inputQ = multiprocessing.JoinableQueue()
	paramSets = defaultdict(dict)
	paramSetId = 0
	for algorithm in algorithms:
		for oFold in oFolds:
			results = defaultdict(lambda: defaultdict(list)) #results[metric][(userk,itemk)]=[1..5]
			resultFolder = expFolder+str(oFold)+'/'+algorithm+'/'
			for filename in os.listdir(resultFolder):
				if filename.startswith('results'):
					with open(resultFolder+filename,'r') as resultFile:
						headerLine = resultFile.readline()
						headerSplit = headerLine.split(',')
						for line in resultFile:
							splitted = line.split(',')
							pid = int(splitted[0])
							method = splitted[1]
							metr = splitted[2]
							score = float(splitted[3])
							paramSetId += 1
							for i,val in enumerate(splitted[4:-1]):
								paramSets[paramSetId][headerSplit[4+i]] = float(splitted[4+i])
							results[metr][paramSetId].append(score)
		for metr in results.keys():
			maxav = 0
			maxParamSetId = 0
			for paramSetId in results[metr].keys():
				av = sum(results[metr][paramSetId])/float(len(results[metr][paramSetId]))
				if av>maxav:
					maxParamSetId = paramSetId
					maxav=av
			pid+=1
			inputQ.put(('','',algorithm,paramSets[maxParamSetId],metr,pid))
			with open(expFolder+'/bestParams.txt','a') as paramsFile:
				paramsFile.write('%s,%s,'%(,algorithm,metr))
				for param in paramSets[maxParamSetId].keys():
					paramsFile.write('%s:%s,'% (param,paramSets[maxParamSetId][param]))
				paramsFile.write(' \n')
	#add poison pills
	for i in xrange(noWorkerNodes):
		inputQ.put(None)
		print 'added poison pill %s out of %s' % (str(i+1),str(noWorkerNodes))

	#start processes
	for i in xrange(noWorkerNodes):
		p = multiprocessing.Process(target=score_worker, args=(inputQ,expFolder))
		p.start()
		print 'started process %s of %s' % (str(i+1),str(noWorkerNodes))

	#wait for all processes to join
	print 'now waiting for all processes to join inputQ'
	inputQ.join()
	print 'joined inputQ, start evaluation'

	
	#summarize results, compute average and stdv
	results = defaultdict(lambda: defaultdict(list))
	for algorithm in algorithms:
		sourceFolder = expFolder+algorithm+'/'
		for filename in os.listdir(sourceFolder):
			if filename.startswith('results'):
				with open(sourceFolder+filename,'r') as sourceFile:
					headerLine = sourceFile.readline()
					for line in sourceFile:
						splitted = line.split(',')
						method = splitted[1]
						algorithm = ''
						if method.startswith('WRMF'):
							algorithm = 'WRMF'
						elif method.startswith('BPRMF'):
							algorithm = 'BPRMF'
						else:
							algorithm = 'PROBLEM'
						metr = splitted[2]
						score = float(splitted[3])
						results[algorithm][metr].append(score)
	summaryString = expFolder+'results.csv'
	with open(summaryString,'a') as summaryFile:
		summaryFile.write('algorithm,metric,av,std,scores\n')
		for method in results.keys():
			for metric in results[method].keys():
				scoreArray = np.array(results[method][metric])
				av = np.mean(scoreArray)
				std = np.std(scoreArray)
				summaryFile.write('%s,%s,%s,%s,' % (method,metric,av,std))
				for score in results[method][metric]:
					summaryFile.write('%s,' % score)
				summaryFile.write(' \n')

################################################################################################
# FUNCTIONS
################################################################################################
def score(data,parentFolder):

	#derived parameters
	oFoldS,iFoldS,algorithm,paramSet,metric,pid = data
	binary_recommender = "./unn/scorer"
	expFolder = parentFolder+oFoldS+iFoldS

	#define in-and outputs
	trainString = expFolder+'train.tsv'
	testString = expFolder+'test.tsv'
	try:
		os.makedirs(expFolder+algorithm+'/')
	except OSError:
		pass
	resultString = expFolder+algorithm+'/'+'scores_nr'+str(pid)+'.csv'

	#execute algorithm
	recommender = algorithm
	recommenderDef = '--recommender='+recommender
	if recommender == 'WRMF':
		num_factors = int(paramSet['num_factors'])
		a = float(paramSet['a'])
		regularization = float(paramSet['regularization'])
		num_iter = int(paramSet['num_iter'])

		options = "--recommender-options=\"num_factors="+str(num_factors)+" alpha="+str(a)+" regularization="+str(regularization)+" num_iter="+str(num_iter)+"\""
		mmlPredString = expFolder+algorithm+'/'+'mmlPred_nr'+str(pid)+'.txt'
		with open(expFolder+algorithm+'/'+'log_nr'+str(pid)+'.txt','w') as logFile:
			subprocess.call('item_recommendation'+' '+'--training-file='+trainString+' '+recommenderDef+' '+'--test-file='+testString+' '+options+' '+'--prediction-file='+mmlPredString,stdout=logFile,stderr=subprocess.STDOUT,shell=True)

		#transform to standard output format
		mmlPredFile = open(mmlPredString,'r')
		resultFile = open(resultString,'w')
		resultFile.write('user,item,'+str(recommender)+'_'+str(num_factors)+'_'+str(a)+'_'+str(regularization)+'_'+str(num_iter)+', \n')
		for line in mmlPredFile:
			splittedLine = line.replace('[',' ').replace(']',' ').replace(':',' ').replace(',',' ').split()
			user = int(splittedLine[0])
			index = 1
			for itemString in splittedLine[1:-1:2]:
				item = int(itemString)
				score = float(splittedLine[index+1])
				index += 2
				resultFile.write(str(user)+','+str(item)+','+str(score)+' \n')
		resultFile.close()
		mmlPredFile.close()
		os.remove(mmlPredString)

	elif recommender == 'BPRMF':
		learn_rate = float(paramSet['learn_rate'])
		num_iter = int(paramSet['num_iter'])
		num_factors = int(paramSet['num_factors'])
		reg_j = float(paramSet['reg_j'])
		reg_i = float(paramSet['reg_i'])
		reg_u = float(paramSet['reg_u'])
		bias_reg = float(paramSet['bias_reg'])

		options = "--recommender-options=\"num_factors="+str(num_factors)+" bias_reg="+str(bias_reg)+" reg_u="+str(reg_u)+" reg_i="+str(reg_i)+" reg_j="+str(reg_j)+" num_iter="+str(num_iter)+" learn_rate="+str(learn_rate)+"\""
		mmlPredString = expFolder+algorithm+'/'+'mmlPred_nr'+str(pid)+'.txt'
		with open(expFolder+algorithm+'/'+'log_nr'+str(pid)+'.txt','w') as logFile:
			subprocess.call('item_recommendation'+' '+'--training-file='+trainString+' '+recommenderDef+' '+'--test-file='+testString+' '+options+' '+'--prediction-file='+mmlPredString,stdout=logFile,stderr=subprocess.STDOUT,shell=True)

		#transform to standard output format
		mmlPredFile = open(mmlPredString,'r')
		resultFile = open(resultString,'w')
		resultFile.write('user,item,'+str(recommender)+'_'+str(num_factors)+'_'+str(bias_reg)+'_'+str(reg_u)+'_'+str(reg_i)+'_'+str(reg_j)+'_'+str(num_iter)+'_'+str(learn_rate)+', \n')
		for line in mmlPredFile:
			splittedLine = line.replace('[',' ').replace(']',' ').replace(':',' ').replace(',',' ').split()
			user = int(splittedLine[0])
			index = 1
			for itemString in splittedLine[1:-1:2]:
				item = int(itemString)
				score = float(splittedLine[index+1])
				index += 2
				resultFile.write(str(user)+','+str(item)+','+str(score)+' \n')
		resultFile.close()
		mmlPredFile.close()
		os.remove(mmlPredString)
		

	#execute evaluation
	eval_kdd(expFolder,algorithm,pid,metric,paramSet)

	return
def score_worker(inputQ,expFolder):
	cont = True
	while cont:
		data = inputQ.get()
		if data == None:
			inputQ.task_done()
			cont = False
			print 'exiting worker after poison pill'
			break
			return
			print '!!!shouldnt be able to print this from score_worker!!!'
		score(data,expFolder)
		inputQ.task_done()
	return

def split_user(negsByUser,itemsByUser,nHoldOut,trainString,testString,groundTruthString,user,delimiter):
	testItems = set()
	trainItems = set()
	inTest = False
	# choose positive testitem
	if len(itemsByUser)>(nHoldOut):
		candidates = list(itemsByUser)
		random.shuffle(candidates)
		for candidate in candidates[0:nHoldOut]:
			testItems.add((candidate,1))
		trainItems = set(candidates[nHoldOut:])
		inTest = True
	elif len(itemsByUser)>1:
		candidates = list(itemsByUser)
		random.shuffle(candidates)
		for candidate in candidates[0:-1]:
			testItems.add((candidate,1))
		trainItems = set(candidates[-1])
		inTest = True
	#write files
	if inTest:
		#add negs to test items
		for neg in negsByUser:
			testItems.add((neg,-1))
		with open(testString,'a') as testFile:
			for testItem in testItems:
				testFile.write(str(user)+delimiter+str(testItem[0])+delimiter+'0'+'\n')
		with open(groundTruthString,'a') as gtFile:
			for testItem in testItems:
				gtFile.write(str(user)+delimiter+str(testItem[0])+delimiter+str(testItem[1])+'\n')
	with open(trainString,'a') as trainFile:
		for item in trainItems:
			trainFile.write(str(user)+delimiter+str(item)+delimiter+str(5)+'\n')
	return trainItems

################################################################################################
# EXECUTE
################################################################################################
if __name__ == '__main__':
	main()
	

