'''
Last update: Feb 2014
koen.verstrepen@gmail.com

Performs experiments with Nearest Neighbors algorithms for KDD2014 submission
'''
from collections import defaultdict
import numpy as np
import operator
import random
import subprocess
import math
import os
import time
import multiprocessing
from eval_kdd import eval_kdd
################################################################################################
# MAIN
################################################################################################
def main():
	#initializaton
	oFolds = [1,2,3,4,5]
	noFolds = len(oFolds)
	algorithms = ['ENSEMBLE_amau']
	delimiter = '\t'
	dataSet = 'yahoo'
	rawDataString = './yahoo_train.txt'
	rawTestString = './yahoo_test.txt'
	smallestPos = 4
	largestNeg = 2
	nHoldOut = 1
	nMinTrain = 1
	expFolder = './exp_kdd_ENSEMBLE_amau_'+dataSet+'_x'+str(noFolds)+'_sp'+str(smallestPos)+'_ln'+str(largestNeg)+'/'
	if not os.path.exists(expFolder):
		os.makedirs(expFolder)
	logString = expFolder+'log.txt'
	noWorkerNodes = 25

	#read raw data
	users = set()
	items = set()
	itemsByUser = defaultdict(set)
	negsByUser = defaultdict(set)
	with open(rawDataString,'r') as rawData:
		for line in rawData:
			splitted = line.replace('::',' ').split()
			user = int(splitted[0])
			item = int(splitted[1])
			rating = int(splitted[2])
			users.add(user)
			items.add(item)
			if rating >= smallestPos:
				itemsByUser[user].add(item)
			if rating <= largestNeg:
				negsByUser[user].add(item)

	#create data splits for determining parameters
	for user in users:
		for oFold in oFolds:
			if not os.path.exists(expFolder+str(oFold)+'/'):
				os.makedirs(expFolder+str(oFold)+'/')
			#oFold determines randomization
			random.seed(oFold)
			#define file paths
			trainString = expFolder+str(oFold)+'/train.tsv'
			testString = expFolder+str(oFold)+'/test.tsv'
			groundTruthString = expFolder+str(oFold)+'/ground_truth.tsv'
			# write files for oFold
			remaining = split_user(negsByUser[user],itemsByUser[user],nHoldOut,trainString,testString,groundTruthString,user,delimiter)

	#define processes
	inputQ = multiprocessing.JoinableQueue()
	#for outerfolds
	pid = 0
	for oFold in oFolds:
		#for algorithms
		for algorithm in algorithms:
			#for parametersets
			if algorithm == 'ENSEMBLE_amau':
				for userk in [5000,2000,1000,100,50]:
					for itemk in [1000,250,100,50,25]:
						lambdas = []
						for lam in [0, 0.25, 0.5, 0.75, 1]:
							pid+=1
							lambdas.append((lam,pid))
						inputQ.put((str(oFold)+'/','',algorithm,userk,itemk,lambdas,'auc_amau_users'))

	#add poison pills
	for i in xrange(noWorkerNodes):
		inputQ.put(None)
		print 'added poison pill %s out of %s' % (str(i+1),str(noWorkerNodes))

	#start processes
	for i in xrange(noWorkerNodes):
		p = multiprocessing.Process(target=score_worker, args=(inputQ,expFolder))
		p.start()
		print 'started process %s of %s' % (str(i+1),str(noWorkerNodes))

	#wait for all processes to join
	print 'now waiting for all processes to join inputQ'
	inputQ.join()
	print 'joined inputQ, start evaluation'

	#create final train, test and ground truth data
	trainString = expFolder+'train.tsv'
	with open(trainString,'w') as trainFile:
		for user in itemsByUser:
			for item in itemsByUser[user]:
				trainFile.write(str(user)+delimiter+str(item)+delimiter+'5'+'\n')
	testString = expFolder+'test.tsv'
	groundTruthString = expFolder+'ground_truth.tsv'
	with open(rawTestString,'r') as rawTestFile:
		with open(testString,'w') as testFile:
			with open(groundTruthString,'w') as gtFile:
				for line in rawTestFile:
					splitted = line.split()
					user = int(splitted[0])
					item = int(splitted[1])
					rating = int(splitted[2])
					if rating >= smallestPos:
						gtFile.write(str(user)+delimiter+str(item)+delimiter+'1'+'\n')
						testFile.write(str(user)+delimiter+str(item)+delimiter+'0'+'\n')
					if rating <= largestNeg:
						gtFile.write(str(user)+delimiter+str(item)+delimiter+'-1'+'\n')
						testFile.write(str(user)+delimiter+str(item)+delimiter+'0'+'\n')

	#define inputs for each (algorithm,metric,outerfold) tuple
	inputQ = multiprocessing.JoinableQueue()
	paramSets = defaultdict(dict)
	paramSetId = 0
	for algorithm in algorithms:
		for oFold in oFolds:
			results = defaultdict(lambda: defaultdict(list)) #results[metric][(userk,itemk)]=[1..5]
			resultFolder = expFolder+str(oFold)+'/'+algorithm+'/'
			for filename in os.listdir(resultFolder):
				if filename.startswith('results'):
					with open(resultFolder+filename,'r') as resultFile:
						headerLine = resultFile.readline()
						headerSplit = headerLine.split(',')
						for line in resultFile:
							splitted = line.split(',')
							pid = int(splitted[0])
							method = splitted[1]
							metr = splitted[2]
							score = float(splitted[3])
							paramSetId += 1
							for i,val in enumerate(splitted[4:-1]):
								paramSets[paramSetId][headerSplit[4+i]] = float(splitted[4+i])
							results[metr][paramSetId].append(score)
		for metr in results.keys():
			maxav = 0
			maxParamSetId = 0
			for paramSetId in results[metr].keys():
				av = sum(results[metr][paramSetId])/float(len(results[metr][paramSetId]))
				if av>maxav:
					maxParamSetId = paramSetId
					maxav=av
			pid+=1
			inputQ.put(('','',algorithm,paramSets[maxParamSetId]['userk'],paramSets[maxParamSetId]['itemk'],[(paramSets[maxParamSetId]['lambda'],pid)],metr))
			with open(expFolder+'/bestParams.txt','a') as paramsFile:
				paramsFile.write('%s,%s,%s,%s,%s,%s,\n'%(oFold,algorithm,metr,paramSets[maxParamSetId]['userk'],paramSets[maxParamSetId]['itemk'],paramSets[maxParamSetId]['lambda']))
	#add poison pills
	for i in xrange(noWorkerNodes):
		inputQ.put(None)
		print 'added poison pill %s out of %s' % (str(i+1),str(noWorkerNodes))

	#start processes
	for i in xrange(noWorkerNodes):
		p = multiprocessing.Process(target=score_worker, args=(inputQ,expFolder))
		p.start()
		print 'started process %s of %s' % (str(i+1),str(noWorkerNodes))

	#wait for all processes to join
	print 'now waiting for all processes to join inputQ'
	inputQ.join()
	print 'joined inputQ, start evaluation'

	#summarize results, compute average and stdv
	results = defaultdict(lambda: defaultdict(list))
	for algorithm in algorithms:
		sourceFolder = expFolder+algorithm+'/'
		for filename in os.listdir(sourceFolder):
			if filename.startswith('results'):
				with open(sourceFolder+filename,'r') as sourceFile:
					headerLine = sourceFile.readline()
					for line in sourceFile:
						splitted = line.split(',')
						method = splitted[1]
						metr = splitted[2]
						score = float(splitted[3])
						results[method][metr].append(score)
	summaryString = expFolder+'restults.csv'
	with open(summaryString,'a') as summaryFile:
		# summaryFile.write('algorithm,metric,av,std,scores\n')
		for method in results.keys():
			for metric in results[method].keys():
				scoreArray = np.array(results[method][metric])
				av = np.mean(scoreArray)
				std = np.std(scoreArray)
				summaryFile.write('%s,%s,%s,%s,' % (method,metric,av,std))
				for score in results[method][metric]:
					summaryFile.write('%s,' % score)
				summaryFile.write(' \n')

################################################################################################
# FUNCTIONS
################################################################################################
def score(data,parentFolder):

	#derived parameters
	oFoldS,iFoldS,algorithm,userk,itemk,lambdas,metric = data
	binary_recommender = "./unn/scorer"
	expFolder = parentFolder+oFoldS+iFoldS
	pids = [pid for lam,pid in lambdas]
	minPid = min(pids)
	maxPid = max(pids)

	#define in-and outputs
	trainString = expFolder+'train.tsv'
	testString = expFolder+'test.tsv'
	try:
		os.makedirs(expFolder+algorithm+'/')
	except OSError:
		pass
	resultString = expFolder+algorithm+'/'+'scores_nr'
	configString = expFolder+algorithm+'/'+'config_nr'+str(minPid)+'_'+str(maxPid)+'.txt'

	#write config file
	with open(configString,'w') as configfile:
		configfile.write(trainString+': \n'
			+testString+': \n'
			+resultString+': \n'
			+algorithm+': \n'
			+str(userk)+':'+str(itemk)+': \n')
		for lam,pid in lambdas:
			configfile.write(str(lam)+':'+str(pid)+':'+' \n')
    #execute algorithm
	with open(expFolder+algorithm+'/'+'log_nr'+str(minPid)+'_'+str(maxPid)+'.txt','w') as logFile:
		subprocess.call([binary_recommender,configString],stdout=logFile,stderr=subprocess.STDOUT)

	#execute evaluation
	for lam,pid in lambdas:
		eval_kdd(expFolder,algorithm,pid,metric,{'userk':userk,'itemk':itemk, 'lambda':lam})

	return
def score_worker(inputQ,expFolder):
	cont = True
	while cont:
		data = inputQ.get()
		if data == None:
			inputQ.task_done()
			cont = False
			print 'exiting worker after poison pill'
			break
			return
			print '!!!shouldnt be able to print this from score_worker!!!'
		score(data,expFolder)
		inputQ.task_done()
	return

def split_user(negsByUser,itemsByUser,nHoldOut,trainString,testString,groundTruthString,user,delimiter):
	testItems = set()
	trainItems = set()
	inTest = False
	# choose positive testitem
	if len(itemsByUser)>(nHoldOut):
		candidates = list(itemsByUser)
		random.shuffle(candidates)
		for candidate in candidates[0:nHoldOut]:
			testItems.add((candidate,1))
		trainItems = set(candidates[nHoldOut:])
		inTest = True
	elif len(itemsByUser)>1:
		candidates = list(itemsByUser)
		random.shuffle(candidates)
		for candidate in candidates[0:-1]:
			testItems.add((candidate,1))
		trainItems = set(candidates[-1])
		inTest = True
	#write files
	if inTest:
		#add negs to test items
		for neg in negsByUser:
			testItems.add((neg,-1))
		with open(testString,'a') as testFile:
			for testItem in testItems:
				testFile.write(str(user)+delimiter+str(testItem[0])+delimiter+'0'+'\n')
		with open(groundTruthString,'a') as gtFile:
			for testItem in testItems:
				gtFile.write(str(user)+delimiter+str(testItem[0])+delimiter+str(testItem[1])+'\n')
	with open(trainString,'a') as trainFile:
		for item in trainItems:
			trainFile.write(str(user)+delimiter+str(item)+delimiter+str(5)+'\n')
	return trainItems

################################################################################################
# EXECUTE
################################################################################################
if __name__ == '__main__':
	main()
	

