'''
created on February 2014 by 
koen.verstrepen@gmail.com

evaluates the recommendations made by an algorithm in a standard form.
'''

from collections import defaultdict
import operator
import random
import subprocess
import math
import os
import time
import multiprocessing

def eval_kdd(expFolder,algorithm,pid,metric,paramSet):
	#this method requires a specific metric since the parameter selection is based on an inner cross validation with respect to this metric

	#initials
	logString = expFolder+algorithm+'/'+'eval_log_metric_'+metric+'_nr_'+str(pid)+'.txt'
	scores = defaultdict(dict)

	#initials
	recommendationsString = expFolder+algorithm+'/'+'scores_nr'+str(pid)+'.csv'

	#noItems & noTrainByUser
	trainString = expFolder+'train.tsv'
	noItems = 0
	items = set()
	noTrainByUser = {}
	with open(trainString,'r') as trainFile:
		for line in trainFile:
			splitted = line.split()
			user = int(splitted[0])
			item = int(splitted[1])
			try:
				noTrainByUser[user] += 1
			except KeyError:
				noTrainByUser[user] = 1
			items.add(item)


	#ground truth and continue noItems
	gtString = expFolder+'ground_truth.tsv'
	gt = {}
	posByUser = defaultdict(set)
	negByUser = defaultdict(set)
	posAll = 0
	negAll = 0
	with open(gtString,'r') as gtFile:
		for line in gtFile:
			splitted = line.replace(',',' ').replace('(',' ').replace(')',' ').split()
			user = int(splitted[0])
			item = int(splitted[1])
			items.add(item)
			truth = int(splitted[2])
			gt[(user,item)] = truth
			if truth == 1:
				posByUser[user].add(item)
				posAll += 1
			elif truth == -1:
				negByUser[user].add(item)
				negAll += 1
	noItems = len(items)
	
	#methods
	methods = set()
	colByMethod = {}
	with open(recommendationsString,'r') as recommendationsFile:
		firstline_splitted = recommendationsFile.readline().replace('\n','').split(',')
		methods = set(firstline_splitted[2:-1]) #list ends as ...,\n
		for i,method in enumerate(firstline_splitted[2:]):
			print 'method: %s --> col: %s' % (method,str(i+2))
			colByMethod[method] = i+2
		recommendationsFile.seek(0)

	#method specific part
	for method in methods:
		if metric == 'arhr':
			eval_aman(expFolder,pid,metric,scores,noItems,noTrainByUser,colByMethod,gt,recommendationsString,logString,method)
		elif metric == 'hr':
			eval_aman(expFolder,pid,metric,scores,noItems,noTrainByUser,colByMethod,gt,recommendationsString,logString,method)
		elif metric == 'auc_amau_users':
			eval_amau(expFolder,pid,metric,scores,noItems,noTrainByUser,colByMethod,gt,recommendationsString,logString,method,posByUser,negByUser)
		elif metric == 'auc_aman_users':
			eval_aman(expFolder,pid,metric,scores,noItems,noTrainByUser,colByMethod,gt,recommendationsString,logString,method)
		elif metric == "aman":
			eval_aman(expFolder,pid,metric,scores,noItems,noTrainByUser,colByMethod,gt,recommendationsString,logString,method)

	# write the results
	outputString = expFolder+algorithm+'/'+'results_'+metric+'_nr'+str(pid)+'.csv'
	# fix order of params and param vals
	paramDef = list()
	paramVal = list()
	for param in paramSet:
		paramDef.append(param)
		paramVal.append(paramSet[param])
	paramDef = tuple(paramDef)
	paramVal = tuple(paramVal)
	with open(outputString,'w') as outputFile:
		outputFile.write('pid,algorithm,metric,score,')
		for param in paramDef:
			outputFile.write('%s,' % param)
		outputFile.write(' \n')
		for metr in scores.keys():
			for method in methods:
				outputFile.write(str(pid)+','+method+','+metr+','+str(scores[metr][method])+',')
				for val in paramVal:
					outputFile.write('%s,' % str(val))
				outputFile.write(' \n')

	#remove file with recommencation scores
	os.remove(recommendationsString)

	return

def eval_aman(expFolder,pid,metric,scores,noItems,noTrainByUser,colByMethod,gt,recommendationsString,logString,method):
	# this method does two passes over the scores file to avoid too much memory usage
	cutoff = 10
	remainingCouples = set()
	for key in gt:
		if gt[key]==1:
			remainingCouples.add(key)
	hitscores = defaultdict(list)
	hitranks = defaultdict(list)
	noOnPar = {}
	noScored = {} 
	with open(recommendationsString) as recommendationsFile:
		recommendationsFile.seek(0)
		firstline = recommendationsFile.readline()
		#pid hit info in memory
		for line in recommendationsFile:
			splitted = line.replace(',',' ').split()
			user = int(splitted[0])
			item = int(splitted[1])
			score = float(splitted[colByMethod[method]])
			truth = 0
			try:
				noScored[user] += 1
			except KeyError:
				noScored[user] = 1
			try:
				truth = gt[(user,item)]
			except KeyError:
				truth = 0
			if truth==1:
				hitscores[user].append(score)
				# print 'hitscore is %s' % score
				remainingCouples.remove((user,item))
		#sort hitscores and initialize hitranks
		for user in hitscores:
			hitscores[user].sort()
			hitranks[user]=range(1,len(hitscores[user])+1)
			hitranks[user].sort(reverse=True)			
		#reset recommendationsFile for next loop		
		recommendationsFile.seek(0)
		firstline = recommendationsFile.readline()
		#compute hitranks per user
		for line in recommendationsFile:
			splitted = line.replace(',',' ').split()
			user = int(splitted[0])
			item = int(splitted[1])
			score = float(splitted[colByMethod[method]])

			truth = 0
			try:
				truth = gt[(user,item)]
			except KeyError:
				truth = 0

			if truth!=1:
				hitscores_local = list()
				try:
					hitscores_local = hitscores[user]
				except KeyError:
					hitscores_local = list()

				noLower = 0
				noEqual = 0
				for hitscore in hitscores_local:
					# print 'score is %s and hitscore is %s' % (score,hitscore)
					if score > hitscore:
						noLower += 1
					elif score == hitscore:
						noEqual += 1

				for i,hitrank in enumerate(hitranks[user][0:noLower]):
					hitranks[user][i]+=1
				relPos = 0
				if noEqual>0:
					relPos = random.sample(xrange(0,noEqual+1),1)[0]
				for i,hitrank in enumerate(hitranks[user][noLower:(noLower+relPos)]):
					hitranks[user][noLower+i] += 1


	# signal the not scored items
	if len(remainingCouples)>0:
		l = list(remainingCouples)
		nr = len(l)
		# print 'example of a hit that was not scored: user %s and item %s for %s' % (l[0][0],l[0][1],method)
		with open(logString,'a') as logFile:
			logFile.write('there are %s hits that did not receive a score.' % (nr))

	#handle hits that were not scored
	unscoredByUser = {}
	for user,item in remainingCouples:
		try:
			unscoredByUser[user] += 1
		except KeyError:
			unscoredByUser[user] = 1
	for user in unscoredByUser:
		minRank=1
		try:
			minRank = noScored[user]+1
		except KeyError:
			minRank = 1
		try:
			extra_ranks = random.sample(range(minRank,noItems-noTrainByUser[user]+1),unscoredByUser[user])
		except:
			print 'user: %s' % user 
			print 'minrank: %s' % minRank
			print 'noItems: %s' % noItems
			print 'noTrainByUser: %s' % noTrainByUser[user]
			print 'unscoredByUser: %s' % unscoredByUser[user]
		extra_ranks.sort(reverse=True)
		hitranks[user] = extra_ranks+hitranks[user]

	if (metric == 'arhr') | (metric == 'aman'):
		#compute arhr
		arhr = 0.0
		noHits = 0.0
		for user in hitranks:
			# print 'hitranks:'
			# print hitranks[user]
			noHits += min(len(hitranks[user]),cutoff)
			for hitrank in hitranks[user]:
				if hitrank<=cutoff:
					arhr+=float(1)/float(hitrank)
		arhr /= float(noHits)
		scores['arhr'][method]=arhr

	if (metric == 'hr') | (metric == 'aman'):
		#compute hr
		hr = 0.0
		noHits = 0.0
		for user in hitranks:
			noHits += min(len(hitranks[user]),cutoff)
			for hitrank in hitranks[user]:
				if hitrank<=cutoff:
					hr+=float(1)
		hr /= float(noHits)
		scores['hr'][method]=hr

	if (metric == 'auc_aman_users') | (metric == 'aman'):
		#compute auc aman
		auc_aman_users = 0.0
		for user in hitranks:
			auc = 0.0
			hitranks[user].sort()
			noHits = len(hitranks[user])
			noMiss = noItems-noTrainByUser[user]-noHits
			for i,hitrank in enumerate(hitranks[user]):
				auc += float(noMiss-hitrank+1+i)
			auc/=float(noHits*noMiss)
			auc_aman_users += float(auc)
		auc_aman_users/=float(len(hitranks.keys()))
		scores['auc_aman_users'][method]=auc_aman_users

	return

def eval_amau(expFolder,pid,metric,scores,noItems,noTrainByUser,colByMethod,gt,recommendationsString,logString,method,posByUser,negByUser):
	# this method does two passes over the scores file to avoid too much memory usage
	remainingCouples = set()
	for key in gt:
		if gt[key]==1:
			remainingCouples.add(key)
	hitscores = defaultdict(list)
	hitranks = defaultdict(list)
	noOnPar = {}
	noScored = {} 
	with open(recommendationsString) as recommendationsFile:
		recommendationsFile.seek(0)
		firstline = recommendationsFile.readline()
		#pid hit info in memory
		for line in recommendationsFile:
			splitted = line.replace(',',' ').split()
			user = int(splitted[0])
			item = int(splitted[1])
			score = float(splitted[colByMethod[method]])
			truth = 0
			try:
				truth = gt[(user,item)]
			except KeyError:
				truth = 0
			if truth != 0:
				try:
					noScored[user] += 1
				except KeyError:
					noScored[user] = 1
				if truth==1:
					hitscores[user].append(score)
					remainingCouples.remove((user,item))
		#sort hitscores and initialize hitranks
		for user in hitscores:
			hitscores[user].sort()
			hitranks[user]=range(1,len(hitscores[user])+1)
			hitranks[user].sort(reverse=True)
		#reset recommendationsFile for next scan		
		recommendationsFile.seek(0)
		firstline = recommendationsFile.readline()
		#compute hitranks per user
		for line in recommendationsFile:
			splitted = line.replace(',',' ').split()
			user = int(splitted[0])
			item = int(splitted[1])
			score = float(splitted[colByMethod[method]])

			truth = 0
			try:
				truth = gt[(user,item)]
			except KeyError:
				truth = 0

			if truth == -1:
				hitscores_local = list()
				try:
					hitscores_local = hitscores[user]
				except KeyError:
					hitscores_local = list()

				noLower = 0
				noEqual = 0
				for hitscore in hitscores_local:
					if score > hitscore:
						noLower += 1
					elif score == hitscore:
						noEqual += 1

				for i,hitrank in enumerate(hitranks[user][0:noLower]):
					hitranks[user][i]+=1
				relPos = 0
				if noEqual>0:
					relPos = random.sample(xrange(0,noEqual+1),1)[0]
				for i,hitrank in enumerate(hitranks[user][noLower:(noLower+relPos)]):
					hitranks[user][noLower+i] += 1


	# signal the not scored items
	if len(remainingCouples)>0:
		l = list(remainingCouples)
		nr = len(l)
		# print 'example of a hit that was not scored: user %s and item %s for %s' % (l[0][0],l[0][1],method)
		with open(logString,'a') as logFile:
			logFile.write('there are %s hits that did not receive a score.' % (nr))

	#handle hits that were not scored
	unscoredByUser = {}
	for user,item in remainingCouples:
		try:
			unscoredByUser[user] += 1
		except KeyError:
			unscoredByUser[user] = 1
	for user in unscoredByUser:
		minRank=1
		try:
			minRank = noScored[user]+1
		except KeyError:
			minRank = 1
		extra_ranks = random.sample(range(minRank,len(posByUser[user])+len(negByUser[user])+1),unscoredByUser[user])
		extra_ranks.sort(reverse=True)
		hitranks[user] = extra_ranks+hitranks[user]

	if metric == 'auc_amau_users':
		#compute auc amau users
		auc_amau_users = 0.0
		participants = 0
		for user in hitranks:
			noHits = len(hitranks[user])
			noMiss = len(negByUser[user])
			if (noHits>0) & (noMiss>0):
				participants += 1
				auc = 0.0
				hitranks[user].sort()
				for i,hitrank in enumerate(hitranks[user]):
					auc += float(noMiss-hitrank+1+i)
				auc/=float(noHits*noMiss)
				auc_amau_users += float(auc)
		auc_amau_users/=float(participants)
		scores['auc_amau_users'][method]=auc_amau_users
	return




